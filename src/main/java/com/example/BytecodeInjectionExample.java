package com.example;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.dynamic.loading.ClassLoadingStrategy;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class BytecodeInjectionExample {

    public static void main(String[] args) throws Exception {


        // ターゲットクラスの名前
        String targetClassName = "com.example.TargetClass";

        // ByteBuddyを使用して新しいメソッドを追加
        DynamicType.Unloaded<?> dynamicType = new ByteBuddy()
                .subclass(Object.class)
                .name(targetClassName)
                .defineMethod("toString", String.class, java.lang.reflect.Modifier.PUBLIC)
                .intercept(Advice.to(MyAdvice.class))
                .make();

        // クラスをロードしてインスタンスを生成
        Class<?> dynamicClass = dynamicType.load(BytecodeInjectionExample.class.getClassLoader(),
                ClassLoadingStrategy.Default.WRAPPER)
                .getLoaded();

        Constructor<?> constructor = dynamicClass.getDeclaredConstructor();

        // インスタンスを生成
        Object targetInstance = constructor.newInstance();
        

        // 追加したメソッドを呼び出す
        Method newMethod = dynamicClass.getDeclaredMethod("toString");
        System.out.println(newMethod.invoke(targetInstance));
 
    }

    // Adviceクラスの定義
    public static class MyAdvice {

        @Advice.OnMethodEnter
        public static void enter() {
            System.out.println("Entering newMethod");
        }

        @Advice.OnMethodExit
        public static void exit() {
            System.out.println("Exiting newMethod");
        }
    }
}
