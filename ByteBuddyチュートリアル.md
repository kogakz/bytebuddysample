<https://bytebuddy.net/#/>

## Creating a class

Byte Buddy によって作成された型は、`ByteBuddy` クラスのインスタンスによって生成されます。`new ByteBuddy()` を呼び出して新しいインスタンスを作成するだけで準備完了です。
できれば、特定のオブジェクトに対して呼び出すことができるメソッドに関する提案が得られる開発環境を使用していることを願っています。
こうすることで、Byte Buddy の javadoc でクラスの API を手動で検索する必要がなく、IDE にプロセスを案内してもらうことができます。
前に述べたように、Byte Buddy は、可能な限り人間が読めるようにすることを目的としたドメイン固有の言語を提供します。したがって、IDE のヒントは、ほとんどの場合、正しい方向を示します。話はこれくらいにして、Java プログラムの実行時に最初のクラスを作成しましょう。

```java
DynamicType.Unloaded<?> dynamicType = new ByteBuddy()
  .subclass(Object.class)
  .make();
```

明らかなように、上記のコード例は、Object 型を拡張する新しいクラスを作成します。
この動的に作成された型は、メソッド、フィールド、またはコンストラクターを明示的に実装せずに、Object を拡張するだけの Java classと同等になります。
お気づきかもしれませんが、動的に生成される型に名前さえ付けていません。これは通常、Java クラスを定義するときに必要です。もちろん、型に明示的に名前を付けることも簡単にできます。

```java
DynamicType.Unloaded<?> dynamicType = new ByteBuddy()
  .subclass(Object.class)
  .name("example.Type")
  .make();
```

### 生成classの命名規則

追記：原文にはないが内容をわかりやすくするため章を分けた

以下の順で決まる

#### デフォルトでの動作の場合

1. 明示的に指定したclassの名前
2. 指定しなかった場合
   - java.lang型：net.bytebuddy.renamedの接頭辞がつく
   - スーパークラス内に`example.Foo$$ByteBuddy$$1376491271`のような名前で作られる

しかし、明示的に名前を付けないとどうなるでしょうか? Byte Buddy は、設定よりも慣習をしっかりと受け継ぎ、便利だと思われるデフォルトを提供します。
型の名前に関しては、デフォルトの Byte Buddy 構成では、動的型のスーパークラス名に基づいてクラス名をランダムに作成する NamingStrategy を提供します。
さらに、名前は**スーパークラスと同じパッケージ内に存在するように定義**されているため、直接スーパークラスのパッケージプライベート メソッドは常に動的型に表示されます。
たとえば、example.Foo という名前の型をサブクラス化した場合、生成される名前は `example.Foo$$ByteBuddy$$1376491271` のようなものになります。
ここで、数値シーケンスはランダムです。
このルールの例外は、Object などの型が存在する java.lang パッケージから型をサブクラス化する場合です。
Java のセキュリティ モデルでは、カスタム型がこの名前空間に存在することは許可されていません。
したがって、そのような型名には、デフォルトの命名戦略によって net.bytebuddy.renamed という接頭辞が付けられます。

#### 命名ルールのカスタマイズ例

このデフォルトの動作は、ユーザーにとって不便な場合があります。
また、構成上の規則の原則のおかげで、必要に応じていつでもデフォルトの動作を変更できます。
ここで ByteBuddy クラスが登場します。新しい ByteBuddy() インスタンスを作成すると、デフォルト構成が作成されます。
この構成でメソッドを呼び出すことで、個々のニーズに合わせてカスタマイズできます。これを試してみましょう:

```java
DynamicType.Unloaded<?> dynamicType = new ByteBuddy()
  .with(new NamingStrategy.AbstractBase() {
    @Override
    protected String name(TypeDescription superClass) {
        return "i.love.ByteBuddy." + superClass.getSimpleName();
    }
  })
  .subclass(Object.class)
  .make();
```

上記のコード例では、型の名前付け戦略がデフォルトの構成とは異なる新しい構成を作成しました。
匿名クラスは、文字列 i.love.ByteBuddy と基本クラスの単純名を単純に連結するために実装されています。
したがって、Object 型をサブクラス化すると、動的型の名前は i.love.ByteBuddy.Object になります。
ただし、独自の命名戦略を作成する場合は注意してください。

- Java 仮想マシンは名前を使用してタイプを区別するため、名前の衝突を避ける必要があります。
- 命名動作をカスタマイズする必要がある場合は、Byte Buddy の組み込み NamingStrategy.SuffixingRandom の使用を検討してください。
これをカスタマイズして、アプリケーションにとってデフォルトよりも意味のあるプレフィックスを含めることができます。

### Domain specific language and immutability

Byte Buddy のドメイン固有言語の動作を確認した後、この言語の実装方法を簡単に見てみる必要があります。
実装に関して知っておく必要がある詳細の 1 つは、言語が`immutable objects`を中心に構築されているということです。
実際のところ、Byte Buddy 名前空間に存在するほぼすべてのクラスは`immutable`になっており、型を不変にできない少数のケースについては、このクラスの javadoc で明示的に言及しています。
Byte Buddy のカスタム機能を実装する場合は、この原則に従うことをお勧めします。

前述の不変性の意味として、たとえば ByteBuddy インスタンスを構成する場合には注意が必要です。たとえば、次のような間違いを犯す可能性があります。

```java
ByteBuddy byteBuddy = new ByteBuddy();
byteBuddy.withNamingStrategy(new NamingStrategy.SuffixingRandom("suffix"));  //変更されたインスタンスを返却するが、元のインスタンスには影響を与えない。
DynamicType.Unloaded<?> dynamicType = byteBuddy.subclass(Object.class).make(); //結果的にデフォルトのNamingStrategyでdynamicTypeは生成される
```

(おそらく) 定義されたカスタム命名戦略 new NamingStrategy.SuffixingRandom("suffix") を使用して動的型が生成されることを期待するかもしれません。
byteBuddy 変数に格納されているインスタンスを変更せずに、`withNamingStrategy`メソッドを呼び出すとカスタマイズされた ByteBuddy インスタンスが返されますが、**このインスタンスは失われます。**
その結果、`dynamicType`は、**最初に作成されたデフォルト構成を使用して作成**されます。

### Redefining and rebasing existing classes

これまでは、Byte Buddy を使用して既存のクラスのサブクラスを作成する方法のみを説明しました。ただし、同じ API を既存のクラスの拡張に使用できます。このような拡張機能は、次の 2 つの異なる形式で利用できます。

- type redefinition 既存メソッドの実装が失われる
- type rebasing 既存メソッドを別名で残して置き換える。

#### type redefinition

クラスを再定義する場合、Byte Buddy では、フィールドとメソッドを追加するか、既存のメソッド実装を置き換えることによって、既存のクラスを変更できます。
ただし、既存のメソッド実装は、別の実装に置き換えられると失われます。たとえば、次の型を再定義する場合

```java
class Foo {
    String bar() {
        return "bar";
    }   
}
```

bar メソッドから **「qux」を返すように、redefinitionする** と、このメソッドが**本来は「bar」を返すメソッドだった**という情報は完全に失われます。

#### type rebasing

クラスをリベースする場合、Byte Buddy はリベースされたクラスのメソッド実装を保持します。
Byte Buddy は、型の再定義を実行するときのようにオーバーライドされたメソッドを破棄するのではなく、そのようなすべてのメソッド実装を、互換性のあるシグネチャを持つ名前が変更されたプライベート メソッドにコピーします。
こうすることで、実装が失われることはなく、リベースされたメソッドは、名前が変更されたメソッドを呼び出すことで**元のコードを引き続き呼び出すことができます。**このようにして、上記のクラス Foo を次のようにリベースできます。

```java
class Foo {
    String bar() {
        return "foo" + bar$original();
    }
    private String bar$original() { //元の実装を別名で残したもの
         return "bar";
    }
}
```

ここで、bar メソッドが最初に「bar」を返したという情報は別のメソッド内に保存されるため、アクセス可能なままになります。
クラスをリベースするとき、Byte Buddy はサブクラスを定義した場合と同様にすべてのメソッド定義を処理します。
つまり、リベースされたメソッドのスーパー メソッド実装を呼び出そうとすると、リベースされたメソッドが呼び出されます。
しかし、その代わりに、最終的にはこの仮想スーパークラスを上に示したリベースされたタイプにフラット化します。

`rebasing(置換)`、`redefinition(再定義)`、または`subclassing(サブクラス化)`は、`DynamicType.Builder inteface`によって定義された同一の API を使用して実行されます。この方法では、たとえばクラスをサブクラスとして定義し、後でその定義を変更して、代わりに置換(リベース)されたクラスを表すことができます。
これは、Byte Buddy のドメイン固有言語の 1 つの単語を変更するだけで実現されます。

```java
new ByteBuddy().subclass(Foo.class)
new ByteBuddy().redefine(Foo.class)
new ByteBuddy().rebase(Foo.class)
```

 このようにして、考えられるいずれかのアプローチの適用は、このチュートリアルの残りの部分で説明する定義プロセスのその後の段階で透過的に処理されます。 サブクラス定義は Java 開発者にとって馴染みのある概念であるため、Byte Buddy のドメイン固有言語に関する以下の説明と例はすべて、サブクラスを作成することによって示されています。 ただし、すべてのクラスは`redefinition(再定義)`や`rebasing(置換)`でも**同様に定義**できることを忘れないようにしてください。

#### Loading a class

これまでのところ、動的タイプを定義して作成しただけで、作成した型を使用しませんでした。 Byte Buddy によって作成された型は、`DynamicType.Unloaded` のインスタンスによって表されます。 名前が示すように、これらの型は Java 仮想マシンには**ロードされません。** 代わりに、Byte Buddy によって作成された型は、Java クラス ファイル形式のバイナリ形式で表されます。 このように、**生成された型をどうするかはあなた次第**です。 たとえば、クラスを生成するだけのビルド スクリプトから Byte Buddy を実行して、Java アプリケーションをデプロイする前に拡張することができます。 この目的のために、DynamicType.Unloaded クラスを使用して、動的型を表すバイト配列を抽出できます。 便宜上、この型には、クラスを特定のフォルダーに保存できる `saveIn(File)` メソッドが追加されています。 さらに、既存の jar ファイルにクラスを挿入すること`inject(File)`もできます。

クラスのバイナリ形式に直接アクセスするのは簡単ですが、残念ながら型をロードするのはより複雑です。 Java では、すべてのクラスは `ClassLoader` を使用してロードされます。 このようなClassLoaderの一例は、Java クラス ライブラリ内で提供されるクラスのロードを担当する`bootstrap class loader`です。 一方、`system class loader`は、Java アプリケーションのクラスパスにクラスをロードする役割を果たします。明らかに、これらの既存の`class loader`は、私たちが作成した**動的クラスを認識しません**。 これを克服するには、ランタイムで生成されたクラスをロードするための他の可能性を見つける必要があります。 Byte Buddy は、すぐに使えるさまざまなアプローチによるソリューションを提供します。

1. 動的に作成された特定のクラスの存在について明示的に通知される新しい ClassLoader を作成するだけです。 JavaのClassLoaderは階層構造になっているため、このClassLoaderを、実行中の Java アプリケーションにすでに存在する特定のClassLoaderの子として定義します。 こうすることで、実行中の Java プログラムのすべての型が、新しい ClassLoader でロードされた動的型に表示されます。

2. 通常、JavaのClassLoaderは、指定された名前の型を直接ロードしようとする前に、親 ClassLoader をクエリします。 これは、親ClassLoaderが同じ名前の型を認識している場合、ClassLoaderは通常、型をロードしないことを意味します。 この目的のために、Byte Buddy は、親をクエリする前に型を単独でロードしようとする`child-first ClassLoader`の作成を提供します。それ以外は、このアプローチは上で説明したアプローチと似ています。 このアプローチは、親ClassLoaderの型を**オーバーライドするのではなく**、この他の型をシャドウすることに注意してください。

3. 最後に、リフレクションを使用して既存の`ClassLoader`に型を注入することができます。通常、`ClassLoader`は名前によって特定の型を提供するように求められます。リフレクションを使用すると、この原則を逆転させ、保護されたメソッドを呼び出して、`ClassLoader`に新しいクラスを注入することができます。この動的クラスをどのように検出するかをクラスローダー自体が知らなくても注入できます。

残念ながら、上記のアプローチには両方の欠点があります。

新しい`ClassLoader`を作成すると、この`ClassLoader`は新しい名前空間を定義します。その意味として、2 つの異なる`ClassLoader`で、同じ名前の 2 つのクラスをロードすることは可能です。どちらのクラスも、同じクラス実装であっても、これら 2 つのクラスはJava 仮想マシンによって等しいと見なされることはありません。 このルールは、Java Packageにも適用されます。これは、`example.Foo`クラスは、`example.Bar`クラスのpackage-privateメソッドにアクセスできないことを意味します。もし両方のクラスが同じクラス・ローダーでロードされていなければ、`example.Bar`は`example.Foo`を拡張したことになります。また、`example.Bar`が`example.Foo`を拡張した場合、オーバーライドされたpackage-privateメソッドは動作しなくなり、元の実装に委譲されます。

クラスがロードされるたびに、そのクラス・ローダーは、別の型を参照しているコード・セグメントが解決されると、このクラスで参照されている型を検索します。この検索は、同じクラス・ローダーに委譲されます。example.Fooとexample.Barの2つのクラスを動的に作成したシナリオを想像してみてください。example.Fooを存在するクラス・ローダーに注入すると、このクラス・ローダーはexample.Barを探そうとするかもしれません。しかし、後者のクラスは動的に作成され、example.Fooクラスを注入したクラス・ローダーには到達できないため、この検索は失敗します。したがって、クラス・ロード中に有効になる循環依存性を持つクラスには、リフレクティブ・アプローチを使用できません。幸いなことに、ほとんどのJVMの実装では、参照されるクラスは最初にアクティブに使用されたときに遅延的に解決されます。また、実際には、Byte Buddyによって作成されるクラスは、通常このような循環性に悩まされることはありません。

一つの動的な型を一度に作成しているため、循環依存の可能性はあまり重要ではないと考えるかもしれません。ただし、型の動的な作成は、いわゆる補助型の作成を引き起こす可能性があります。これらの型は、作成中の動的な型にアクセスするために、Byte Buddyによって自動的に作成されます。補助型については、次のセクションで詳しく学びますが、今のところ心配しないでください。ただし、このため、できるだけ既存のクラスローダーに直接注入する代わりに、特定のクラスローダーを作成して動的に作成されたクラスをロードすることをお勧めします。

DynamicType.Unloadedを作成した後、この型はClassLoadingStrategyを使用してロードできます。このような戦略が提供されていない場合、Byte Buddyは提供されたクラスローダーに基づいてそのような戦略を推測し、デフォルトでは型が反映を使用して注入されないブートストラップクラスローダーのために新しいクラスローダーを作成します。Byte Buddyは、前述の概念のいずれかに従ういくつかのクラスローディング戦略を提供しています。これらの戦略はClassLoadingStrategy.Defaultで定義されており、WRAPPER戦略は新しいラッピングClassLoaderを作成し、CHILD_FIRST戦略は子優先のセマンティクスを持つ類似のクラスローダーを作成し、INJECTION戦略は反映を使用して動的な型を注入します。WRAPPERおよびCHILD_FIRST戦略は、いわゆるマニフェストバージョンでも利用可能であり、クラスがロードされた後も型のバイナリ形式が保持されます。これらの代替バージョンでは、クラスローダーのクラスのバイナリ表現がClassLoader::getResourceAsStreamメソッドを介してアクセス可能です。ただし、これにはこれらのクラスローダーがクラスの完全なバイナリ表現への参照を維持する必要があり、これがJVMのヒープ上のスペースを消費します。したがって、バイナリ形式に実際にアクセスする予定がない場合は、マニフェストバージョンのみを使用する必要があります。INJECTION戦略は反映を介して機能し、ClassLoader::getResourceAsStreamメソッドのセマンティクスを変更する可能性がないため、自然とマニフェストバージョンでは使用できません。

このようなクラスのローディングが実際にどのように行われるかを見てみましょう。

```java
Class<?> type = new ByteBuddy()
  .subclass(Object.class)
  .make()
  .load(getClass().getClassLoader(), ClassLoadingStrategy.Default.WRAPPER)
  .getLoaded();
```

上記の例では、クラスを作成してロードしました。クラスのロードには、前述のように一般的に適しているWRAPPER戦略を使用しました。最後に、getLoadedメソッドは、現在ロードされている動的なクラスを表すJavaクラスのインスタンスを返します。

クラスをロードする際には、事前定義されたクラスローディング戦略が現在の実行コンテキストのProtectionDomainを適用して実行されることに注意してください。また、すべてのデフォルト戦略は、withProtectionDomainメソッドを呼び出すことで明示的なプロテクションドメインの指定を提供しています。明示的なプロテクションドメインを定義することは、セキュリティマネージャを使用する場合や署名済みのJARファイルで定義されたクラスを操作する場合に重要です。

#### Reloading a class

前のセクションで、既存のクラスを`redefine(再定義)`または`rebase(リベース)`するためにByte Buddyを使用する方法を学びました。ただし、Javaプログラムの実行中に特定のクラスが既にロードされていないことを保証することは通常不可能です。さらに、(Byte Buddyは現在、ロードされたクラスを引数として受け入れるのみであり、将来のバージョンでは未ロードのクラスと同様に操作するために既存のAPIが使用できるようになります。)Java仮想マシンのHotSwap機能のおかげで、ロードされた後であっても既存のクラスを`redefine(再定義)`することができます。この機能は、Byte BuddyのClassReloadingStrategyによってアクセス可能になっています。クラスFooを`redefine(再定義)`することで、この戦略をデモンストレーションしましょう。

```java
class Foo {
  String m() { return "foo"; }
}
 
class Bar {
  String m() { return "bar"; }
}
```

Byte Buddyを使用すると、クラスFooを簡単に再定義してBarにすることができます。HotSwapを使用すると、この再定義は既存のインスタンスにも適用されます。

```java
ByteBuddyAgent.install();
Foo foo = new Foo();
new ByteBuddy()
  .redefine(Bar.class)
  .name(Foo.class.getName())
  .make()
  .load(Foo.class.getClassLoader(), ClassReloadingStrategy.fromInstalledAgent());
assertThat(foo.m(), is("bar"));
```

HotSwapは、いわゆるJavaエージェントを使用してのみアクセスできます。このようなエージェントは、Java仮想マシンの起動時に-javaagentパラメータを使用して指定するか、パラメータの引数にはMaven CentralからダウンロードできるByte BuddyのエージェントJARを指定します。ただし、JavaアプリケーションがJava仮想マシンのJDKインストールから実行される場合、Byte BuddyはByteBuddyAgent.installOnOpenJDK()を使用してアプリケーション起動後にもJavaエージェントをロードできます。クラスの再定義は主にツールやテストを実装するために使用されるため、これは非常に便利な代替手段です。Java 9以降、JDKのインストールなしでランタイムでエージェントのインストールも可能です。

上記の例について最初に直感に反するように思える点の一つは、Bar型を再定義するようにByte Buddyに指示されているが、最終的にはFoo型が再定義されるという事実です。Java仮想マシンは型をその名前とクラスローダーで識別します。したがって、BarをFooに名前を変更し、この定義を適用することで、最終的にはBarに名前を変更した型を再定義します。もちろん、異なる型の名前を変更せずに直接Fooを再定義することも可能です。

ただし、JavaのHotSwap機能を使用する場合、1つの大きな欠点があります。HotSwapの現行の実装では、再定義されたクラスがクラスの再定義前後で同じクラススキーマを適用する必要があります。これは、クラスをリロードする際にメソッドやフィールドを追加することは許可されていないことを意味します。Byte Buddyは、クラスを再ベースするたびに元のメソッドのコピーを定義するため、クラスの再ベースはClassReloadingStrategyに対して機能しないことを既に議論しました。また、クラスの再定義は、明示的なクラス初期化メソッド(クラス内の静的ブロック)を持つクラスに対しても機能しません。なぜなら、この初期化子も別のメソッドにコピーする必要があるからです。残念ながら、OpenJDKはHotSwapの機能拡張から撤退してしまったため、この制限を回避する方法はありません。その間、Byte BuddyのHotSwapサポートは、有用であると思われる場合に限り、特定のケースで使用できます。それ以外の場合は、クラスの再ベースと再定義は、例えばビルドスクリプトから既存のクラスを拡張する際に便利な機能となります。

#### Working with unloaded classes

Java の HotSwap 機能の限界についてこのように認識すると、リベースおよび再定義命令の唯一の意味のある適用はビルド時であると考える人もいるかもしれません。 ビルド時の操作を適用すると、このクラスのロードが JVM の別のインスタンスで完了するという理由だけで、処理されたクラスが最初のクラスのロードの前にロードされないと主張できます。 ただし、Byte Buddy は、まだロードされていないクラスも同様に操作できます。 このため、Byte Buddy は Java のリフレクション API を抽象化して、たとえば Class インスタンスが TypeDescription のインスタンスによって内部的に表現されるようにします。 実際のところ、Byte Buddy は、TypeDescription インターフェイスを実装するアダプターによって提供されたクラスを処理する方法のみを知っています。 この抽象化に対する大きな利点は、クラスに関する情報を ClassLoader によって提供する必要がなく、他のソースによって提供できることです。

Byte Buddy は、TypePool を使用してクラスの TypeDescription を取得する標準的な方法を提供します。 もちろん、このようなプールのデフォルト実装も提供されます。 この TypePool.Default 実装は、クラスのバイナリ形式を解析し、それを必要な TypeDescription として表します。 ClassLoader と同様に、これもカスタマイズ可能な、表現されたクラスのキャッシュを維持します。 また、通常はクラスのバイナリ形式を ClassLoader から取得しますが、このクラスをロードするように指示することはありません。

Java 仮想マシンは、最初の使用時にのみクラスをロードします。 結果として、たとえば次のようなクラスを安全に再定義できます。

```java
package foo;
class Bar { }
```

他のコードを実行する前に、プログラムの起動時にすぐに:

```java
class MyApplication {
  public static void main(String[] args) {
    TypePool typePool = TypePool.Default.ofSystemLoader();
    Class bar = new ByteBuddy()
      .redefine(typePool.describe("foo.Bar").resolve(), // do not use 'Bar.class'  ※2
                ClassFileLocator.ForClassLoader.ofSystemLoader())
      .defineField("qux", String.class) // we learn more about defining fields later
      .make()
      .load(ClassLoader.getSystemClassLoader(), ClassLoadingStrategy.Default.INJECTION) //※1
      .getLoaded(); 
    assertThat(bar.getDeclaredField("qux"), notNullValue());
  }
}
```

アサーション ステートメントで最初に使用する前に再定義されたクラスを明示的にロード(※1)することで、JVM の組み込みクラスのロードを回避します。 このようにして、foo.Bar の再定義された定義がロードされ、アプリケーションのランタイム全体で使用されます。 ただし、TypePool を使用して説明を提供する場合、クラス リテラルによってクラスを参照しないことに注意してください。 foo.Bar にクラス リテラルを使用した場合(※2)、JVM は再定義する前にこのクラスをロードしてしまい、再定義の試みは無効になります。 また、アンロードされたクラスを操作する場合、クラスのクラス ファイルを検索できるようにする ClassFileLocator をさらに指定する必要があることに注意してください。 上の例では、実行中のアプリケーションのクラス パスをスキャンしてそのようなファイルを探すクラス ファイル ロケーターを作成するだけです。

### Creating Java agents

アプリケーションが大きくなり、よりモジュール化されると、特定のプログラム ポイントでそのような変換を適用することは、当然、強制するのが面倒な制約になります。 そして、そのようなクラスの再定義をオンデマンドで適用する、より良い方法が実際にあります。 JavaAgentを使用すると、Java アプリケーション内で実行されるクラス読み込みアクティビティを直接インターセプトできます。 [JavaAgent](https://docs.oracle.com/javase/8/docs/api/java/lang/instrument/package-summary.html)は、リンクされた`java.lang.instrument`のjavadocで説明されているように、この jar ファイルのマニフェストファイルで指定されたエントリ ポイントを持つ単純な jar ファイルとして実装されます。 Byte Buddy を使用すると、AgentBuilder を使用して`JavaAgent`を簡単に実装できます。 以前に ToString という名前の単純なアノテーションを定義したと仮定すると、次のようにエージェントの premain メソッドを実装するだけで、すべてのアノテーション付きクラスに toString メソッドを実装するのは簡単です。

例：
`ToString`というアノテーションをつけたクラスのtoStringメソッドの値を`transformed`に変換する

```java
class ToStringAgent {
  public static void premain(String arguments, Instrumentation instrumentation) {
    new AgentBuilder.Default()
        .type(isAnnotatedWith(ToString.class))
        .transform(new AgentBuilder.Transformer() {
      @Override
      public DynamicType.Builder transform(DynamicType.Builder builder,
                                              TypeDescription typeDescription,
                                              ClassLoader classloader) {
        return builder.method(named("toString"))
                      .intercept(FixedValue.value("transformed"));
      }
    }).installOn(instrumentation);
  }
}
```

上記の AgentBuilder.Transformer を適用すると、すべての`ToStringアノテーション`付きクラスの `toString メソッド`は変換された値を返すようになります。

Byte Buddy の DynamicType.Builder については次のセクションですべて学習します。今のところ、このクラスについては心配しないでください。

もちろん、上記のコードは、単純で意味のないアプリケーションになります。ただし、この概念を正しく使用すると、アスペクト指向プログラミングを簡単に実装するための強力なツールが得られます。

`Agent`を使用する場合、`bootstrap class loader`によってロードされたクラスを`instrument`することも可能であることに注意してください。 ただし、これにはある程度の準備が必要です。 まず、`bootstrap class loader`は null 値で表されるため、リフレクションを使用してこのクラス ローダーにクラスをロードすることができなくなります。 ただし、これは、クラスの実装をサポートするためにヘルパー クラスをインストルメント化されたクラスのクラス ローダーにロードするために必要になる場合があります。クラスを`bootstrap class loader`にロードするために、Byte Buddy は jar ファイルを作成し、これらのファイルを`bootstrap class loader`のロード パスに追加できます。 ただし、これを可能にするには、**これらのクラスをディスクに保存する**必要があります。 これらのクラスのフォルダーは、`enableBootstrapInjectionコマンド`を使用して指定できます。このコマンドは、クラスを追加するために Instrumentation interfaceのインスタンスも取得します。 インストルメント化されたクラスによって使用されるすべてのユーザー クラスも、Instrumentation interfaceを使用して可能なブートストラップ検索パス上に配置する必要があることに注意してください。

### Loading classes in Android applications

省略

### Working with generic types

Byte Buddy は、Java プログラミング言語で定義された`generic types`を処理します。 `generic types`は、`generic types`の消去のみを処理する Java ランタイムでは考慮されません。 ただし、`generic types`は依然として Java クラス ファイルに埋め込まれており、Java リフレクション API によって公開されます。 したがって、`generic types`情報は他のライブラリやフレームワークの動作に影響を与える可能性があるため、生成されたクラスにジェネリック情報を含めることが合理的な場合もあります。 `generic types`情報の埋め込みは、クラスが永続化され、Java コンパイラーによってライブラリとして処理される場合にも重要です。

クラスをサブクラス化するとき、インターフェイスを実装するとき、またはフィールドまたはメソッドを宣言するとき、Byte Buddy は上記の理由により、消去されたクラスの代わりに Java タイプを受け入れます。 `generic types`は、TypeDescription.Generic.Builder を使用して明示的に定義することもできます。 Java `generic types`と型消去の重要な違いの 1 つは、型変数のコンテキスト上の意味です。 ある型によって定義された特定の名前の型変数は、別の型が同じ型変数を同じ名前で宣言した場合に、同じ型を示すとは限りません。 したがって、Byte Buddy は、Type インスタンスがライブラリに渡されるときに、生成された型またはメソッドのコンテキストで型変数を示すすべての`generic types`を再バインドします。

Byte Buddy は、型の作成時にブリッジ メソッドも透過的に挿入します。 ブリッジ メソッドは、ByteBuddy インスタンスのプロパティである MethodGraph.Compiler によって解決されます。 デフォルトのメソッド グラフ コンパイラは Java コンパイラと同様に動作し、クラス ファイルの`generic types`情報を処理します。 ただし、Java 以外の言語の場合は、別のメソッドのグラフ コンパイラが適切な場合があります。

## Fields and methods

前のセクションで作成したほとんどの型では、フィールドやメソッドが定義されていませんでした。 ただし、Object をサブクラス化すると、作成されたクラスはそのスーパークラスで定義されたメソッドを継承します。 この Java トリビアを検証し、動的型のインスタンスで toString メソッドを呼び出してみましょう。 作成したクラスのコンストラクターをリフレクティブに呼び出すことで、インスタンスを取得できます。

```java
String toString = new ByteBuddy()
                      .subclass(Object.class)
                      .name("example.Type")
                      .make()
                      .load(getClass().getClassLoader())
                      .getLoaded()
                      .newInstance() // Java reflection API
                      .toString();
```

Object#toString メソッドの実装は、インスタンスの完全修飾クラス名とインスタンスのハッシュ コードの 16 進表現を連結したものを返します。
実際、作成されたインスタンスで toString メソッドを呼び出すと、`example.Type@340d1fa5` のようなものが返されます。

もちろん、これで終わりではありません。 動的クラスを作成する主な動機は、新しいロジックを定義できることです。
これがどのように行われるかを示すために、簡単なことから始めましょう。

toString メソッドをオーバーライドして Hello World! を返したいと考えています。

以前のデフォルト値の代わりに:

```java
String toString = new ByteBuddy()
                      .subclass(Object.class)
                      .name("example.Type")
                      .method(named("toString")).intercept(FixedValue.value("Hello World!"))
                      .make()
                      .load(getClass().getClassLoader())
                      .getLoaded()
                      .newInstance()
                      .toString();
```

コードに追加した行には、Byte Buddy のドメイン固有言語での 2 つの命令が含まれています。

最初の命令は、オーバーライドするメソッドを任意の数選択できるメソッドです。
この選択は、オーバーライド可能な各メソッドをオーバーライドするかどうかを決定する述語として機能する ElementMatcher を渡すことによって適用されます。

Byte Buddy には、ElementMatchers クラスに収集された事前定義された`method matchers`が多数付属しています。
通常は、結果のコードがより自然に読み取れるように、このクラスを静的にインポートします。

このような静的インポートは、正確な名前でメソッドを選択する名前付きメソッドマッチャーを使用した上記の例でも想定されています。なお、事前定義された`method matchers`を組み合わせて構成することができます。このようにすると、次のようにメソッドの選択をさらに詳細に説明できます。

```java
named("toString").and(returns(String.class)).and(takesArguments(0))
```

この`method matcher`では、完全な Java signature(名前、戻り値、引数)によって toString メソッドを説明しているため、特定のメソッドのみに一致します。
ただし、指定されたコンテキストでは、元の`method matcher`で十分であるような、異なるシグネチャを持つ toString という名前のメソッドが他に存在しないことがわかります。

toString メソッドを選択した後、2 番目の命令である`intercept`は、指定された選択のすべてのメソッドをオーバーライドする実装が決定されます。
メソッドの実装方法を知るために、この命令には Implementation 型の引数が 1 つ必要です。

上の例では、Byte Buddy に同梱されている`FixedValue implementation`を利用しています。
このクラスの名前が示すように、実装では常に指定された値を返すメソッドが実装されます。

このセクションの少し後の方で、`FixedValue implementation`について詳しく説明します。
ここでは、メソッドの選択方法についてもう少し詳しく見てみます。

これまでのところ、`intercept`でオーバーライドしたメソッドは 1 つだけです。
ただし、実際のアプリケーションでは、状況はさらに複雑になる可能性があり、さまざまなメソッドをオーバーライドするためにさまざまなルールを適用することが必要になる場合があります。

このようなシナリオの例を見てみましょう。

---
サンプルを読むための補足：原文にはない説明です。

- named : 名前に基づいてクラスやメソッドをマッチングする
- isDeclaredBy: 特定のクラスによって宣言されたメソッドをマッチングします。
- isOverriddenFrom: 特定のクラスからオーバーライドされたメソッドをマッチングします。
- takesArguments: 特定の数の引数を取るメソッドをマッチングします。
- returns: 特定の型を返すメソッドをマッチングします。
- isPublic, isPrivate, isProtected, isPackagePrivate: メソッドの可視性に基づいてマッチングします。
- isAnnotatedWith : 特定のアノテーションが付けられたクラスやメソッドをマッチングします。

`and`や`or`を使用して組み合わせることができる。

また、`not( 何かmather)`とすると、そのmatherに該当しないものということを表す。

---

```java
class Foo {
  public String bar() { return null; }
  public String foo() { return null; }
  public String foo(Object o) { return null; }
}
 
// 優先度は後勝ちなので、Three!,Two!,One!の順。(ややこしいが)
Foo dynamicFoo = new ByteBuddy()
                    .subclass(Foo.class)
                     //Fooクラスのメソッドの返却値をすべてOne!にする
                    .method(isDeclaredBy(Foo.class)).intercept(FixedValue.value("One!")) 
                     //fooメソッドの返却値をTwo!
                    .method(named("foo")).intercept(FixedValue.value("Two!"))
                    //fooで引数を一つ持つメソッドの返却値をThree!にする
                    .method(named("foo").and(takesArguments(1))).intercept(FixedValue.value("Three!")) 
                    .make()
                    .load(getClass().getClassLoader())
                    .getLoaded()
                    .newInstance();
```

上の例では、メソッドをオーバーライドするための 3 つの異なるルールを定義しました。

コードを調査すると、以下のようになっています。

- 最初のルールが Foo によって定義されたメソッド、つまりサンプル クラスの 3 つのメソッドすべてに一致します。
- 2 番目のルールは、前の選択のサブセットである foo という名前の両方のメソッドに一致します。
- 最後のルールは、前の選択をさらに縮小した foo(Object) メソッドにのみ一致します。

複数の条件に一致する場合、Byte Buddy はどのルールをどのメソッドに適用するかをどのように決定するかを説明します。

Byte Buddy は、メソッドをオーバーライドするためのルールをスタック形式で編成します。
これは、メソッドをオーバーライドするための**新しいルールを登録するたび**に、そのルールがこの**スタックの最上位にプッシュ**され、さらに優先順位が高くなる新しいルールが追加されるまで**常に最初に適用される**ことを意味します。

上の例の場合、以下のようになります。

- bar() メソッド
  - 最初にnamed("foo").and(takesArguments(1))と照合されますが一致しません。
  - 次にnamed("foo")と照合されますが、これとも一致しません。
  - 最後に、isDeclaredBy(Foo.class) マッチャーは一致するため、bar() メソッドは One! を返すようオーバーライドされます。

- foo() メソッド
  - 最初にnamed("foo").and(takesArguments(1)) と照合されますが、引数が欠落しているため照合は失敗します。
  - この後、named("foo") に一致するため、 foo() メソッドがTwo! を返すようオーバーライドされます。

- foo(Object) メソッド
  - named("foo").and(takesArguments(1))に一致するため、Three! を返すようオーバーライドされます。

このような構成のため、**より具体的なmethod matcher**を最後に登録する必要があります。
そうしないと、後から登録される**具体性の低いmethod matcher**によって、その前に定義したルールが適用されなくなる可能性があります。

**ここはignoreMethodnに関して説明が理解できなかったため、原文と併記しておく**
Note that the ByteBuddy configuration allows to define an ignoreMethod property.
ByteBuddy 構成では、ignoreMethod プロパティを定義できることに注意してください。

notをつけるケースのこと？ ignoreMethodで検索して見つけられなかった。

Methods that are successfully matched against this method matcher are never overridden.
このメソッド マッチャーとの照合に成功したメソッドはオーバーライドされません。

By default, Byte Buddy does not override any synthetic methods.
デフォルトでは、Byte Buddy は合成メソッドをオーバーライドしません。

シナリオによっては、スーパータイプまたはインターフェイスのメソッドをオーバーライドしない新しいメソッドを定義することが必要になる場合があります
Byte Buddyを使用することも可能です。

(この辺りは読みやすいよう箇条書きに変更しています。)

- メソッドの定義
  - defineMethodでsignatureを指定します。
  - メソッドを定義した後は、`method matcher`によって識別されたメソッドの場合と同様に、実装を提供するように求められます。
  - メソッドの定義後に登録された`method matcher`は、前に説明したスタッキング原則によってこの実装に取って代わられる可能性があることに注意してください。

- フィールドの定義
  - defineField を使用して、特定のタイプのフィールドを定義できます。
  - Java では、フィールドは決してオーバーライドされず、隠ぺいのみが可能です。
  - このため、フィールドマッチングなどは利用できません。

メソッドがどのように選択されるかについての知識があれば、これらのメソッドを実装する方法を学ぶ準備が整います。
この目的のために、Byte Buddy に同梱される事前定義されたimplementationを見ていきます。

独自のimplementationを定義する方法は、独自のセクションで説明します。
非常に特殊なimplementationを必要とするユーザーのみを対象としています。

### A closer look at fixed values

すでに、FixedValue の実装が実際に動作しているのを見てきました。

名前が示すように、FixedValue によって実装されたメソッドは、単に提供されたオブジェクトを返します。
クラスは、次の 2 つの異なる方法でそのようなオブジェクトを記憶できます。

FixedValueはクラスの定数プールに書き込まれます。
定数プールは、Java クラス ファイル形式内のセクションであり、任意のクラスのプロパティを記述する多数のステートレス値が含まれています。
定数プールは主に、クラス名やメソッド名などのクラスのプロパティを記憶するために必要です。
これらのreflectionで取得可能なプロパティに加えて、定数プールには、クラスのメソッドまたはフィールド内で使用される文字列またはプリミティブ値を格納するための余地があります。文字列とプリミティブ値に加えて、クラス プールには他の型への参照も格納できます。

値はクラスの静的フィールドに保存されます。
ただし、これを実現するには、クラスが Java 仮想マシンにロードされた後でフィールドに指定された値を割り当てる必要があります。

この目的のために、動的に作成されたすべてのクラスには、そのような明示的な初期化を実行するように構成できる TypeInitializer が付属しています。

DynamicType.Unloaded のロードを指示すると、Byte Buddy はその型初期化子を自動的にトリガーして、クラスを使用できるようにします。
したがって、通常は型初期化子について心配する必要はありません。

ただし、**動的クラスを Byte Buddy の外部でロードする場合**は、これらのクラスの**ロード後に型初期化子を手動で実行**することが重要です。

それ以外の場合、静的フィールドにはこの値が割り当てられていないため、FixedValueは、たとえば、必要な値の代わりに null を返します。
ただし、多くの動的型は明示的な初期化を必要としない場合があります。

したがって、クラスの型初期化子は、その isAlive メソッドを呼び出すことによって、その活性度を問い合わせることができます。
TypeInitializer を手動でトリガーする必要がある場合は、DynamicType インターフェイスによって公開されます。

FixedValue#value(Object) でメソッドを実装すると、Byte Buddy はパラメータの型を分析します。

- 可能であれば動的型のクラス プールに格納されるように定義します。
- そうでない場合は静的フィールドに値を格納します。

ただし、値がクラス プールに格納されている場合、選択したメソッドによって返されるインスタンスのオブジェクト ID が異なる可能性があることに注意してください。

したがって、FixedValue#reference(Object) を使用して、常にオブジェクトを静的フィールドに格納するように Byte Buddy に指示できます。

後者のメソッドはオーバーロードされており、フィールドの名前を 2 番目の引数として指定できます。
それ以外の場合、フィールド名はオブジェクトのハッシュ コードから自動的に導出されます。

この動作の例外は null 値です。 Null 値はフィールドに格納されることはなく、単にそのリテラル式によって表されます。
この文脈でのタイプ セーフティについて疑問に思うかもしれません。 明らかに、無効な値を返すメソッドを定義できます。

```java
new ByteBuddy()
    .subclass(Foo.class)
    .method(isDeclaredBy(Foo.class)).intercept(FixedValue.value(0))
    .make();
```

Java の型システム内のコンパイラによるこの無効な実装を防ぐことは困難です。
代わりに、型が作成され、String を返すメソッドへの整数の不正な代入が有効になると、Byte Buddy は IllegalArgumentException をスローします。
Byte Buddy は、作成されたすべての型が正当な Java 型であることを保証するために最善を尽くし、不正な型の作成中に例外をスローすることで高速に失敗します。

Byte Buddy の割り当て動作はカスタマイズ可能です。
繰り返しますが、Byte Buddy は、Java コンパイラの割り当て動作を模倣するまともなデフォルトのみを提供します。
その結果、Byte Buddy では、型をそのスーパー型のいずれかに割り当てることができます。その際には、プリミティブ値をラッパー表現に変換(boxing)するか、ラッパー表現をプリミティブ値に変換(unboxing)することも考慮されます。

ただし、Byte Buddy は現在、ジェネリック型を完全にはサポートしていません。型の消去のみを考慮することに注意してください。
したがって、Byte Buddy がヒープ汚染を引き起こす可能性※があります。
(※追記：同じようなJavaTypeを大量に作ってしまって、ヒープをひっ迫させる可能性があるということ？)

事前定義された"assigner(※)"を使用する代わりに、Java プログラミング言語では暗黙的ではない型変換が可能な独自のAssignerをいつでも実装できます。このチュートリアルの最後のセクションでは、このようなカスタムする方法を詳しく説明します。
とりあえず、FixedValueからwithAssignerを呼び出せば、独自のAssignerをつかえることだけ覚えておいてください。

※追記：Assignerは、メソッドの戻り値や引数の型を変換する役割を果たすもの。

### Delegating a method call

多くのシナリオでは、メソッドから固定値を返すだけでは当然不十分です。
柔軟性を高めるために、Byte Buddy はメソッド呼び出しに最大限に自由に反応できる MethodDelegationを提供します。

MethodDelegationは、「動的型の外に存在する別メソッドに処理を転送する」メソッドを動的に定義します。
こうすることで、動的クラスのロジックを純粋にJavaコードで表現することができ、別のメソッドへのバインディングのみがコード生成によって実現されます。

詳細について説明する前に、MethodDelegation の使用例を見てみましょう。

(追記：概念図を入れたほうが分かりやすそう)

```java
class Source {
  public String hello(String name) { return null; }
}
 
class Target {
  public static String hello(String name) {
    return "Hello " + name + "!";
  }
}
 
String helloWorld = new ByteBuddy()
                        .subclass(Source.class)
                        .method(named("hello")).intercept(MethodDelegation.to(Target.class))
                        .make()
                        .load(getClass().getClassLoader())
                        .getLoaded()
                        .newInstance()
                        .hello("World");
```

この例では、メソッドが null値ではなくHello World! を返すように、Source#hello(String) メソッドの呼び出しを Target 型に委任しています。 このために、MethodDelegationは、`Target.class`内の呼び出し可能なメソッドから**最も一致するメソッドを識別**します。
上記の例では、`Target.class`はメソッドのパラメーター、戻り値の型、名前が Source#name(String) のパラメーターと同一である単一の静的メソッドのみを定義しているためこれは簡単です。

実際のケースでは、呼び出すメソッドを決定はより複雑な場合が高いあります。
では、複数の候補がある場合、Byte Buddy はどの方法を選択するのでしょうか?

これを説明するために、ターゲット クラスが次のように定義されていると仮定します。

```java
// 追記：　コメントは文章の内容をソース上でわかるよう、追加したものです。
class Target {
  // メソッド名は書き換え対象のメソッドとは異なるものでもOK!
  public static String intercept(String name) { return "Hello " + name + "!"; } //これが選ばれる
  public static String intercept(int i) { return Integer.toString(i); }         // 引数の型が一致しないので選ばれない
  public static String intercept(Object o) { return o.toString(); }             // 引数の互換性はあるが選ばれない
}
```

お気づきかもしれませんが、Targetクラスのメソッドの名前がすべて"intercept"となっています。Byte Buddy では、(delegationからの呼び出し先である)target methodに、(Target処理を置き換えたい対象である) と同じ名前を付ける必要はありません。 これについては後程、詳しく説明します。

それよりも重要なのは、Target の定義を変更して前の例を実行すると、hello(String) は、intercept(String) にバインドされます。
しかし、それはなぜでしょうか?

明らかに、intercept(int) メソッドはソース メソッドの String 引数を受け取ることができないため、一致する可能性があるとみなされません。
ただし、これはバインドされる可能性がある intercept(Object) メソッドには当てはまりません。

このあいまいさを解決するために、Byte Buddy は、最も具体的なパラメータ タイプとのメソッド バインディングを選択することにより、Java コンパイラを再び模倣します。

Java コンパイラがオーバーロードされたメソッドのバインディングをどのように選択するかを思い出してください。

String は Object よりも具体的であるため、最終的に 3 つの選択肢の中から intercept(String) クラスが選択されます。

これまでの情報から、メソッド バインディング アルゴリズムはかなり厳格な性質のものであると考えるかもしれません。
しかし、私たちはまだその全容を語っていません。これまでのところ、**Convention over Configuration (CoC:設計よりも規約)**の別の例が紹介されただけです。デフォルトの動作が実際の要件に適合しない場合に変更することも可能です。

MethodDelegationはアノテーションと連動して機能し、パラメータのアノテーションがどの値をターゲットメソッドに割り当てるべきかを決定します。ただし、注釈が見つからない場合、Byte Buddy はパラメータを `@Argument` で注釈が付けられているかのように扱います。

注釈が明示的に追加されない場合、n の値は注釈付きパラメータのインデックスに設定されます。

 このルールに従って、Byte Buddy は次のように扱います。

```java
void foo(Object o1, Object o2)
```

すべてのパラメータに次のように注釈が付けられているかのようになります。

```java
void foo(@Argument(0) Object o1, @Argument(1) Object o2)
```

その結果、ソースメソッドの最初と 2 番目の引数がインターセプターに割り当てられます。

インターセプトされたメソッドが少なくとも 2 つのパラメーターを宣言していない場合、またはアノテーション付きパラメーターの型がインスツルメントされたメソッドのパラメーターの型から割り当てられない場合、問題のインターセプター メソッドは破棄されます。

`@Argument` アノテーション以外にも、MethodDelegation で使用できる事前定義されたアノテーションがいくつかあります。

- `@AllArguments`
  - アノテーションを持つパラメータは配列型である必要があり、ソース メソッドのすべての引数を含む配列が割り当てられます。
  - 利用条件：　すべてのソース メソッド パラメーターが配列のコンポーネント タイプに割り当て可能であること
  - 利用条件を満たさない場合： 現在のターゲット メソッドはソース メソッドにバインドされる候補とは見なされません。

- `@This`
  - Delegateするために生成された動的な型のインスタンスを、パラメータとして渡すために使います。
  - よくある利用目的 : 対象インスタンスのフィールドにアクセスしたい場合
  - 利用条件：  注釈付きパラメータに、動的型のインスタンスを割り当てらること。
  - 利用条件を満たさない場合： 対象のメソッドはソースメソッドにバインドされる候補とは見なされません。
                              (下の例だとObjectとしているため何でも受けられるが、ちゃんとした型を指定したら、対象が絞られる)
  - 補足： `@This`で受けとったインスタンスのメソッドを呼び出した場合、対象メソッドがバイトコードが書き換え対象の場合は、書き換え後のメソッドが呼び出されます。オーバーライドされる前のメソッドを呼び出した場合は、@Superを使用する必要があります。

```java
// 仮の理解しして追記(間違っている可能性あり)
// delegateで呼び出されるメソッド側のパラメータに@Thisで動的生成されたクラスを渡す
public static String intercept(@This Object obj) {
    //　動的生成されたクラスは、もともとのクラスのメソッドを持つ形で公開される
    //  そのため、移譲先であるinterceptメソッド内から、
    //  obj.呼び出したいメソッドを呼び出すことで
    // 元のインスタンスの状態を読み取ったり、変更したりすることができる。
}
```

`@Origin` の注釈が付けられたパラメータは、Method、Constructor、Executable、Class、MethodHandle、MethodType、String、または int のいずれかのタイプで使用する必要があります。

パラメータの型に応じて、現在インストルメントされている元のメソッドまたはコンストラクタへのメソッドまたはコンストラクタ参照、または動的に作成されたクラスへの参照が割り当てられます。`Java 8` を使用する場合、`interceptor`内で`Executable type`を使用してメソッドまたはコンストラクターの参照を受け取ることもできます。

```java
public class Example {
    public static class Interceptor {
        public static String intercept(@Origin Method method) {
            return method.getName();
        }
    }

    public static void main(String[] args) throws Exception {
        String toString = new ByteBuddy()
                .subclass(Object.class)
                .method(named("toString"))
                .intercept(MethodDelegation.to(Interceptor.class))
                .make()
                .load(Example.class.getClassLoader())
                .getLoaded()
                .newInstance()
                .toString();

        System.out.println(toString); // prints "toString"
    }
}
```

注釈付きパラメータが文字列の場合、そのパラメータにはメソッドの toString メソッドが返す値が割り当てられます。

```java
public class Example {
    public static class Interceptor {
        public static String intercept(@Origin String method) {
            return method;
        }
    }

    public static void main(String[] args) throws Exception {
        String toString = new ByteBuddy()
                .subclass(Object.class)
                .method(named("toString"))
                .intercept(MethodDelegation.to(Interceptor.class))
                .make()
                .load(Example.class.getClassLoader())
                .getLoaded()
                .newInstance()
                .toString();

        System.out.println(toString); // prints "public java.lang.String java.lang.Object.toString()"
    }
}
```

一般に、可能な場合はメソッド識別子としてこれらの String 値を使用することをお勧めします。
また、`methodオブジェクト`の検索により実行時に重大なオーバーヘッドが発生するため、`methodオブジェクト`の使用は避けてください。

このオーバーヘッドを回避するために、 `@Origin` アノテーションは、再利用のためにそのようなインスタンスをキャッシュするためのプロパティも提供します。

`MethodHandle` と `MethodType` はクラスの定数プールに格納しますが、これらの定数を使用する場合は`Java バージョン 7`が必要です。
別のオブジェクトでインターセプトされたメソッドをリフレクションを使った動的呼び出しをするかわりに、`@Pipe`アノテーションを使用することをお勧めします。`@Pipe`アノテーションについては、このセクションで後ほど説明します。

int 型のパラメーターで `@Origin` アノテーションを使用すると、インストルメント化されたメソッドの修飾子をあらわす値が割り当てられます。
追記：private,public,staticなど

追加：実行例(Modifireは`java.lang.reflect.Modifier`)

```java
public class Example {
    public static class Interceptor {
        public static String intercept(@Origin int modifiers) {
            return Modifier.toString(modifiers);
        }
    }

    public static void main(String[] args) throws Exception {
        String toString = new ByteBuddy()
                .subclass(Object.class)
                .method(named("toString"))
                .intercept(MethodDelegation.to(Interceptor.class))
                .make()
                .load(Example.class.getClassLoader())
                .getLoaded()
                .newInstance()
                .toString();

        System.out.println(toString); // prints "public"
    }
}
```

Byte Buddy では、事前定義された注釈を使用するほかに、1 つまたは複数の ParameterBinder を登録することで独自のアノテーションを定義できます。このチュートリアルの最後のセクションでは、このようなカスタマイズについて説明します

これまで説明した 4 つのアノテーションのほかに、動的型のメソッドのsuper implementation(継承元の実装)のアクセスを許可する 2 つの事前定義されたアノテーションが存在します。

この方法で、`dynamic type`は、メソッド呼び出しのログ記録などをクラスに追加できます。

`@SuperCall` アノテーションを使うと、`super implementation`(継承元の実装)を動的クラスの外部(=Delegationでの移譲先)から呼び出せます。

例です。

```java
class MemoryDatabase {
  public List<String> load(String info) {
    return Arrays.asList(info + ": foo", info + ": bar");
  }
}
 
class LoggerInterceptor {
  // 追記：Callble<List<String>> → List<String>を返り値とするメソッド なのでList<String> loadが割り当てられる
  // CallableがExceptionをスローするインタフェースのため、Exceptionをthrowする宣言になっている。
  public static List<String> log(@SuperCall Callable<List<String>> zuper)
      throws Exception {
    System.out.println("Calling database");
    try {
      return zuper.call();
    } finally {
      System.out.println("Returned from database");
    }
  }
}
 
MemoryDatabase loggingDatabase = new ByteBuddy()
  .subclass(MemoryDatabase.class)
  .method(named("load")).intercept(MethodDelegation.to(LoggerInterceptor.class))
  .make()
  .load(getClass().getClassLoader())
  .getLoaded()
  .newInstance();
```

上記サンプルでは、次のことがはっきりわかります。継承元メソッドの呼び出しがされていること。そのために、`LoggerInterceptor`に`Callable型`のインスタンスとして引数として渡されていること。それを使ってオーバーライドされていない `MemoryDatabase#load(String)`の実装が呼び出されていること。
このヘルパークラスは、 Byte Buddyの用語では`AuxiliaryType`と呼ばれます。`AuxiliaryType`は、Byte Buddyが必要に応じて生成し、クラス生成された`DynamicType interface`から直接アクセス可能です。
これらの`AuxiliaryType`があるため、`DynamicType`を生成すると、いくつかの追加クラスを生成することがあります。追加クラスはオリジナルクラスの実装を支援（アクセスの補助？）するものです。
最後に、`@SuperCall annotation`は、`Runnable型` にも利用可能です。 ただし、`Runnable型`の場合は、元のメソッドが値を返す場合でも、返り値は削除されます。

あなたは疑問に思っているかもしれません。どのようにして、`AuxiliaryType`が通常はJavaで隠ぺいされている他のクラスの継承元メソッドを呼び出せるのか？と。

よく見てみると、しかしながら、このふるまいはまったく一般的で、次のJavaソースコードをコンパイルした時に得られるコンパイルされたコードと類似しています。

```java
class LoggingMemoryDatabase extends MemoryDatabase {
 
  // LoggingMemoryDatabaseを呼び出すための引数をメンバーとして受け取り、
  // callメソッドを呼ぶと、事前に受け取った値をloadに渡して呼び出す。
  private class LoadMethodSuperCall implements Callable {
 
    private final String info;
    private LoadMethodSuperCall(String info) {
      this.info = info;
    }
 
    @Override
    public Object call() throws Exception {
      return LoggingMemoryDatabase.super.load(info);
    }
  }
 
  // exception宣言無し
  @Override
  public List<String> load(String info) {
    return LoggerInterceptor.log(new LoadMethodSuperCall(info));
  }
}

```

ただし、場合によっては、メソッドの元の呼び出し時に割り当てられた引数とは異なる引数を使用して継承元のメソッドを呼び出したい場合があります。 Byte Buddy でも @Super アノテーションを使用することでこれが可能です。

このアノテーションを使うと別の`AuxiliaryType`が生成されます。その`AuxiliaryType`は、対象の`DynamicType`の`super class`または`interface`を拡張するものです。

前に説明した通り、`AuxiliaryType`はすべてのメソッドをオーバーライドして、動的型のスーパー実装を呼び出します。
このようにして、前の例に挙げていた`logger interceptor`は、実際の呼び出しを変更するための実装を行えます。

```java
class ChangingLoggerInterceptor {
  public static List<String> log(String info, @Super MemoryDatabase zuper) {
    System.out.println("Calling database");
    try {
      return zuper.load(info + " (logged access)");
    } finally {
      System.out.println("Returned from database");
    }
  }
}
```

なお、`@Super`アノテーションが付けられたパラメータに割り当てられたインスタンスは、生成された`DynamicType`のインスタンスのIDとは異なります。
したがって、パラメーターを使用してアクセスできるインスタンス フィールドは、実際のインスタンスのフィールドを反映しません。 さらに、`AuxiliaryType`のオーバーライド不可能なメソッドは呼び出しを委任せず、元の実装を保持するため、呼び出し時に不合理な動作が発生する可能性があります。 最後に、`@Super`アノテーションが付けられたパラメータが関連する動的型のスーパー型を表していない場合、メソッドは(継承元のクラス)のメソッドのいずれのバインディング ターゲットとも見なされません。
※この翻訳は自信がない。

`@Super`アノテーションでは任意の型の使用が許可されるため、この型の構築方法に関する情報の提供が必要になる場合があります。

デフォルトでは、Byte Buddy はクラスのデフォルトコンストラクターを使用しようとします。これは、オブジェクト型を暗黙的に拡張するインターフェイスに対して常に機能します。

ただし、`DynamicType`のスーパークラスを拡張する場合、SuperClassがデフォルトのコンストラクターさえ提供しない可能性があります。デフォルトコンストラクタが提供されない、または`AuxiliaryType`の作成に特定のコンストラクターを使用する必要がある場合、`@Super`アノテーションを使用すると、そのパラメーターの型をアノテーションのコンストラクターパラメータープロパティとして設定することで、別のコンストラクターを特定できます。

このコンストラクターは、対応するデフォルト値を各パラメーターに割り当てることによって呼び出されます。

代わりに、classを生成するのに`Super.Instantiation.UNSAFE strategy`を使うことも可能です。`Super.Instantiation.UNSAFE strategy`は、コンストラクタを呼ばずに`AuxiliaryType`を生成するために`Java内部クラス`を利用します。

この戦略は Oracle 以外の JVM に必ずしも移植できるわけではなく、将来の JVM リリースでは使用できなくなる可能性があることに注意してください。
ただし、現時点では、この安全でないインスタンス化戦略で使用される内部クラスは、ほぼすべての JVM 実装に存在します。

さらに、LoggerInterceptorの例では、logメソッドで`checked exception`を宣言していることにすでに気づいたかもしれません。
※`checed exception`： コンパイル時にチェックされる例外

一方、このメソッドを呼び出すインストルメント化されたソースメソッド(=LoggingMemoryDatabaseのloadメソッド）では、`checked exception`を宣言しません。

通常、Java コンパイラはそのような呼び出しのコンパイルを拒否します。
ただし、コンパイラとは対照的に、Java ランタイムは`checked exception`を`unchecked counterparts(チェックされていない例外)`と異なる扱いをせず（追記：つまり同じ扱いとして）、この呼び出しを許可します。
※追記： `unchecked counterparts` Runtime Exceptinなどのコンパイル時にチェックされない例外

このため、`checked exception`を無視し、その使用に完全な柔軟性を与えることにしました。
ただし、動的に作成されたメソッドから未宣言の`checked exception`をスローする場合は、そのような例外が発生するとアプリケーションのユーザーが混乱する可能性があるため、注意してください。

`method delegation`には、もう 1 つ気になる注意点があります。静的型付けはメソッドの実装に最適ですが、型を厳密にすると、コードの再利用を制限する可能性があります。
その理由を理解するには、次の例を考えてください。

```java
class Loop {
  public String loop(String value) { return value; }
  public int loop(int value) { return value; }
}
```

上記のクラスのメソッドは、2つの類似した`singnature`を持っていますが、型の互換性がないため、1つのインターセプターメソッドを使って、通常、両方のメソッドを書きかえることはできません。
代わりに、静的型チェックを満たすためにのみ、異なるシグネチャを持つ 2 つの異なるターゲット メソッドを提供する必要があります。

この制限を克服するために、Byte Buddy ではメソッドとメソッド パラメーターに @RuntimeType アノテーションを付けることができ、実行時の型キャストを優先して厳密な型チェックを一時停止するように Byte Buddy に指示します。

```java
class Interceptor {
  @RuntimeType
  public static Object intercept(@RuntimeType Object value) {
    System.out.println("Invoked method with: " + value);
    return value;
  }
}
```

上記の`target method(移譲先メソッド)`を使用すると、両方の`source methods(移譲元メソッド)`に対して単一のインターセプトメソッドを提供できるようになりました。

なお、`Byte Buddy`はプリミティブ値を`box化`することも、`unbox化`することもできます。
ただし、`@RuntimeType`を使用すると型安全性が放棄されるという代償が伴い、互換性のない型が混在すると`ClassCastException`が発生しうることに注意ください。

`@SuperCall` と同等のものとして、Byte Buddy には `@DefaultCall`アノテーションが付属しており、メソッドのスーパーメソッドを呼び出す代わりに`DefaultMethod`を呼び出すことができます。

この`@DefaultCall`アノテーションを持つメソッドは、`移譲元のメソッド`が実際に`interface定義でdefaultメソッドとして宣言`されている場合のみ、バインディングの対象と見なされます。

追加: 例を追加

```java
public interface DefaultMethodInterface {
    // ポイント1. defualtメソッドとして宣言
    default String output() {
        return "Default Method";
    }
}

public class DefaultMethodClass implements DefaultMethodInterface {

    // ポイント2. defaultメソッドをオーバーライド
    @Override
    public String output() {
        return "Overridden Method";
    }
}

public class Interceptor {

    // ポイント3. @DefaultCallアノテーションでメソッドを受け取るようにしていため、
    //             オーバーライドされた実装ではなく
    //             対象インタフェースのdefault宣言されているメソッドが設定される
    @RuntimeType
    public static Object intercept(@DefaultCall Callable<?> zuper) throws Exception {
        return zuper.call();
    }
}

public class Main {
    public static void main(String[] args) throws Exception {
        DefaultMethodClass instance = new ByteBuddy()
            .subclass(DefaultMethodClass.class)
            .method(named("output"))
            .intercept(MethodDelegation.to(Interceptor.class))
            .make()
            .load(Main.class.getClassLoader(), ClassLoadingStrategy.Default.WRAPPER)
            .getLoaded()
            .newInstance();

        System.out.println(instance.output());  // Prints "Default Method"
    }
}
```

同様に、`@SuperCall`アノテーションは、インストルメントされたメソッドが非抽象スーパーメソッドを定義していない(追記:抽象メソッドしかない)場合、メソッドのバインドを防ぎます。

ただし、特定の型でデフォルトメソッドを呼び出したい場合は、特定のインターフェイスを指定して`@DefaultCall`の`targetType`プロパティを指定できます。

追記 : 例を追加

```java
public interface DefaultMethodInterface {
  default String defaultMethod() {
    return "Default Method";
  }
}

public class Interceptor {
  @RuntimeType
  public static Object intercept(@DefaultCall(targetType = DefaultMethodInterface.class) Callable<?> zuper) throws Exception {
    return zuper.call();
  }
}
```

この仕様では、Byte Buddy は、指定されたインターフェイスタイプのdefaultメソッドが存在する場合、それを呼び出すプロキシインスタンスを挿入します。
それ以外の場合、パラメーターアノテーションを持つ`target method(移譲先メソッド)`は移譲ターゲットとは見なされません。
追記： ざっくりまとめると、条件に合致したときだけ移譲先メソッドが呼び出される。

明らかに、デフォルトのメソッド呼び出しは、Java 8 以降のクラス ファイル バージョンで定義されたクラスでのみ使用できます。
同様に、`@Super`アノテーションに加えて、特定のデフォルト メソッドを明示的に呼び出すためのプロキシを挿入する`@Default`アノテーションがあります。

任意の`MethodDelegation`でカスタムアノテーションを定義および登録できることはすでに述べました。 `Byte Buddy`には、すぐに使用できるアノテーションが1つ付属していますが、明示的にインストールして登録する必要があります。

`@Pipe`アノテーションを使用すると、インターセプトされたメソッド呼び出しを別のインスタンスに転送できます。
By using the @Pipe annotation, you can forward an intercepted method invocation to another instance.

`Java8より前のJavaクラスライブラリ`には、`Function型`を定義する適切な`Interface型`が付属していないため、`@Pipe`アノテーションは`MethodDelegation`に事前登録されません。オブジェクトを引数として受け取り、別のオブジェクトを結果値として返す単一の非staticメソッドを、型に明示的に提供する必要があります。

メソッド型がオブジェクト型によってバインドされている限り、ジェネリック型も使用できることに注意してください。
もちろん、`Java8` を使用している場合は、Function タイプも利用可能です。

パラメーターの引数でメソッドを呼び出す場合、Byte Buddy はパラメーターをメソッドの宣言型にキャストし、元のメソッド呼び出しと同じ引数を使用してインターセプトされたメソッドを呼び出します。ただし、例を見る前に、`Java5` 以降で使用できるカスタム型を定義してみましょう。

```java
interface Forwarder<T, S> {
  T to(S target);
}
```

この型を使用すると、メソッド呼び出しを既存のインスタンスに転送することで、上記の MemoryDatabase へのアクセスをログに記録するための新しいソリューションを実装できます。

```java
class ForwardingLoggerInterceptor {
 
  private final MemoryDatabase memoryDatabase; // constructor omitted
 
  public List<String> log(@Pipe Forwarder<List<String>, MemoryDatabase> pipe) {
    System.out.println("Calling database");
    try {
      return pipe.to(memoryDatabase);
    } finally {
      System.out.println("Returned from database");
    }
  }
}
 
MemoryDatabase loggingDatabase = new ByteBuddy()
  .subclass(MemoryDatabase.class)
  .method(named("load")).intercept(MethodDelegation.withDefaultConfiguration()
    .withBinders(Pipe.Binder.install(Forwarder.class)))
    .to(new ForwardingLoggerInterceptor(new MemoryDatabase()))
  .make()
  .load(getClass().getClassLoader())
  .getLoaded()
  .newInstance();
```

上の例では、呼び出しをローカルに生成した他のインスタンスにフォワードしているだけです。
ただし、型をサブクラス化してメソッドをインターセプトするよりも優れている点は、このアプローチでは既存のインスタンスを拡張できることです。さらに、通常は、クラスレベルで静的インターセプターを登録するのではなく、インスタンスレベルでインターセプターを登録します。

これまで、`MethodDelegation`を使った実装について多くを見てきました。ただし、先に進む前に、Byte Buddy がターゲット メソッドを選択する方法について詳しく見ていきたいと思います。

`Byte Buddy`がパラメーターの型を比較す​​ることで最も具体的なメソッドを解決する方法についてはすでに説明しましたが、それだけではありません。`Byte Buddy`は、ソースメソッドへのバインディングに適した候補メソッドを特定した後、`AmbiguityResolver`のチェーンに解決を委任します。繰り返しになりますが、`Byte Buddy`のデフォルトを補完したり、置き換えたりできる独自の`AmbiguityResolver`を自由に実装できます。このような変更がなければ、`AmbiguityResolver`チェーンは、次のルールを同じ順序で適用することによって、一意のターゲットメソッドを識別しようとします。

- `@BindingPriority`アノテーションを付けることで、メソッドに明示的な優先度を割り当てることができます。あるメソッドの優先順位が別のメソッドよりも高い場合、常に優先順位の高いメソッドが優先順位の低いメソッドよりも優先されます。さらに、`@IgnoreForBinding`によって注釈が付けられたメソッドは、ターゲット メソッドとは見なされません。
- 移譲元メソッドと、移譲先メソッドの名前が同じ場合、この移譲先メソッドは、名前が異なる他の移譲先メソッドよりも優先されます。
- `@Argument`を使用して 2 つのメソッドが移譲元メソッドの同じパラメータをバインドする場合、最も具体的なパラメータタイプを持つメソッドが考慮されます。
  - このコンテキストでは、アノテーションが明示的に提供されるか、パラメーターにのテーションを付けないことで暗黙的に提供されるかは問題ではありません。
    - どのメソッドを優先するかのアルゴリズムは、オーバーロードされたメソッドの呼び出しを解決するための Java コンパイラのアルゴリズムと同様に機能します。
    - 2 つの型が同じように固有である場合、より多くの引数をバインドするメソッドがターゲットとみなされます。
  - メソッドの優先度決定時にパラメータのタイプを考慮せずにパラメータに引数を割り当てる必要がある場合は、アノテーションの`bindingMechanic`属性を`BindingMechanic.ANONYMOUS`に設定することで可能になります。さらに、メソッド優先度の決定アルゴリズムが機能するには、非匿名パラメータが各移譲先メソッドのインデックス値ごとに一意である必要があることに注意してください。
- ターゲット メソッドに別のターゲット メソッドよりも多くのパラメーターがある場合、前者のメソッドが後者よりも優先されます。

これまでは、`MethodDelegation.to(Target.class)`のように特定のクラスに名前を付けることで、メソッド呼び出しを静的メソッドに委任するだけでした。ただし、インスタンスメソッドまたはコンストラクターに委任することもできます。

- `MethodDelegation.to(new Target())`を呼び出すことで、`Target`クラスのインスタンスメソッドのいずれかにメソッド呼び出しを委任できます。
  - これには、Object クラスで定義されたメソッドなど、インスタンスのクラス階層内の任意の場所で定義されたメソッドが含まれることに注意してください。
  - 任意の`MethodDelegation`で `filter(ElementMatcher)`を呼び出して`MethodDelegation`にフィルターを適用することで、候補メソッドの範囲を制限したい場合があります。`ElementMatcher型`は、`Byte Buddy`のドメイン固有言語内で`移譲元メソッド`を選択するために以前に使用されていたものと同じです。
  - `MethodDelegation`の対象となるインスタンスは`staticフィールド`に格納されます。固定値の定義と同様に、これには`TypeInitializer`の定義が必要です。
  - `staticフィールド`に委任を保存する代わりに、`MethodDelegation.toField(String)`によってフィールドの使用を定義することもできます。この場合、引数はすべての`MethodDelegation`の転送先となるフィールド名を指定します。このような動的クラスのインスタンスでメソッドを呼び出す前に、必ずこのフィールドに値を割り当ててください。それ以外の場合、`MethodDelegation`により NullPointerException が発生します。
- `MethodDelegation`を使用して、特定の型のインスタンスを構築できます。 `MethodDelegation.toConstructor(Class)`を使用すると、インターセプトされたメソッドを呼び出すと、指定されたターゲット型の新しいインスタンスが返されます。

学習したばかりのとおり、MethodDelegation はバインディング ロジックを調整するためにアノテーションを検査します。これらのアノテーションは Byte Buddy に固有のものですが、これは、アノテーションが付けられたクラスが何らかの形で Byte Buddy に依存することを意味するものではありません。代わりに、Java ランタイムは、クラスがロードされるときにクラスパス上で見つからないアノテーション タイプを単純に無視します。これは、動的クラスの作成後に Byte Buddy が必要なくなることを意味します。これは、クラスパス上に Byte Buddy がなくても、動的クラスとそのメソッド呼び出しを委任する型を別の JVM プロセスにロードできることを意味します。

- `@Empty`: このアノテーションを適用すると、Byte Buddy はパラメータタイプのデフォルト値を挿入します。プリミティブ型の場合、これは数値のゼロに相当し、参照型の場合、これは null になります。このアノテーションの使用は、インターセプターのパラメーターを無効にすることを目的としています。
- `@StubValue`: このアノテーションを使用すると、アノテーション付きパラメータにインターセプトされたメソッドのスタブ値が挿入されます。参照戻り型および void メソッドの場合、`null値` が挿入されます。プリミティブ値を返すメソッドの場合、同等の`Boxingされた型`の`0`が挿入されます。これは、`@RuntimeType`アノテーションの使用中にオブジェクト型を返す汎用インターセプターを定義するときに組み合わせると便利です。注入された値を返すことにより、メソッドはプリミティブな戻り値の型を正しく考慮しながらスタブとして動作します。
- `@FieldValue`: このアノテーションは、インストルメント化された型のクラス階層内のフィールドを特定し、そのフィールドの値をアノテーション付きパラメータに挿入します。アノテーション付きパラメータに対して互換性のある型の表示フィールドが見つからない場合、ターゲットメソッドはバインドされません。
- `@FieldProxy`: このアノテーションを使用して、`Byte Buddy` は指定されたフィールドにアクセサーを挿入します。アクセスされるフィールドは、その名前によって明示的に指定することも、インターセプトされたメソッドが`getterメソッド`または`setterメソッド`を表す場合には、`getterメソッド`または`setterメソッド`の名前から派生することもできます。このアノテーションを使用するには、`@Pipe`アノテーションと同様に、明示的にインストールして登録する必要があります。
- `@Morph`: このアノテーションは `@SuperCall`アノテーションと非常によく似た働きをします。ただし、このアノテーションを使用すると、`Superメソッド`の呼び出しに使用する引数を指定できます。 `@Morph`アノテーションを使用するにはすべての引数のBoxingとUnboxingが必要なため、元の呼び出しとは異なる引数でスーパー メソッドを呼び出す必要がある場合にのみ、このアノテーションを使用する必要があることに注意してください。特定のスーパー メソッドを呼び出したい場合は、タイプセーフなプロキシを作成するために @Super アノテーションを使用することを検討してください。このアノテーションを使用するには、`@Pipe` アノテーションと同様に、明示的にインストールして登録する必要があります。
- `@SuperMethod`: このアノテーションは、メソッドから割り当て可能なパラメータタイプでのみ使用できます。割り当てられたメソッドは、元のコードの呼び出しを可能にする合成アクセサー メソッドに設定されます。このアノテーションを使用すると、プロキシ クラスに対してパブリック アクセサーが作成され、セキュリティ マネージャーを渡さずにスーパー メソッドの外部呼び出しが可能になることに注意してください。
  - 追記： synthetic method=合成メソッド(オーバーライドするときに、戻り値の型をそのサブクラス型に変更した場合に、コンパイラが元の型のままのメソッドを自動的に生成して、オーバーライドしたメソッドに処理を移譲する。コンパイラが自動生成したメソッドのことを合成メソッドという。
  - [参考サイト1](https://www.sakatakoichi.com/entry/20080805/1217925680)
  - [参考サイト2](https://www.ne.jp/asahi/hishidama/home/tech/java/covariant.html)
- `@DefaultMethod`: `@SuperMethod`に似ていますが、デフォルトのメソッド呼び出し用です。デフォルト メソッド呼び出しの可能性が 1 つしかない場合、デフォルト メソッドは一意の型で呼び出されます。それ以外の場合は、型をアノテーション プロパティとして明示的に指定できます。

### Calling a super method

名前が示すように、`SuperMethodCall`を使用してメソッドの継承元の実装を呼び出すことができます。一見すると、スーパーの実装を唯一呼び出すだけでは実装が変わるわけではなく、既存のロジックを単に複製するだけであるため、あまり有用ではないように見えます。ただし、メソッドをオーバーライドすることで、メソッドとそのパラメータのアノテーションを変更できます。これについては次のセクションで説明します。ただし、Java でスーパーメソッドを呼び出すもう1つの根拠は、スーパータイプまたは独自のタイプの別のコンストラクターを常に呼び出す必要があるコンストラクターの定義です。

これまでは、`DynamicType`のコンストラクターは常にその直接のスーパー型のコンストラクターに似ていると単純に想定していました。例として、次のように呼び出すことができます。

So far we simply assumed that the constructors of a dynamic type would always resemble the constructors of its direct super type. As an example, we could call

```java
new ByteBuddy()
  .subclass(Object.class)
  .make()
```

直接のスーパー コンストラクター (Object のデフォルト コンストラクター) を単に呼び出すように定義された単一のデフォルト コンストラクターを使用して、Object のサブクラスを作成します。ただし、この動作は Byte Buddy によって規定されていません。

代わりに、上記のコードは呼び出しを省略したものになります。

```java
new ByteBuddy()
  .subclass(Object.class, ConstructorStrategy.Default.IMITATE_SUPER_TYPE)
  .make()
```

ここで、`ConstructorStrategy` は、任意のクラスに対して事前定義されたコンストラクターのセットを作成する役割を果たします。上の例で使っている`ConstructorStrategy.Default.IMITATE_SUPER_TYPE`は、`DynamicType`の直接スーパークラスの呼び出し可能な各コンストラクターをコピーするものです。
そのほかにも、事前定義されたStrategyが存在します。

- コンストラクターをまったく作成しないもの
- 直接スーパークラスのデフォルトコンストラクターを呼び出すデフォルトコンストラクターを作成するもの。スーパークラスのデフォルトコンストラクターが存在しない場合は例外をスローします。
- スーパークラスのパブリックコンストラクターのみを模倣するコンストラクターを作成するもの

Java クラスファイルフォーマット内では、コンストラクターは一般にメソッドと異なるものではないため、`Byte Buddy`ではコンストラクターをメソッドと同様に扱うことができます。ただし、コンストラクターには、Java ランタイムによって受け入れられる別のコンストラクターのハードコーディングされた呼び出しが含まれている必要があります。このため、`SuperMethodCall`以外のほとんどの事前定義実装は、コンストラクターに適用するときに有効な Java クラスを作成できません。

（追加：以下は原文を意訳しています。）
ただし、カスタム実装を使用してコンストラクターを定義することはでき、それには2つの方法があります。

- カスタム `ConstructorStrategy`を実装する。
- `defineConstructor`メソッドを使って`Byte Buddy`のドメイン固有言語内で個別のコンストラクターを定義する

さらに、より複雑なコンストラクターをすぐに定義できるように、Byte Buddy に新機能を追加する予定です。

原文
> However, by using custom implementations, you are able to define your own constructors by either implementing a custom ConstructorStrategy or by defining an individual constructor within Byte Buddy's domain specific language using the defineConstructor method. Furthermore, we are planing to add new features to Byte Buddy for defining more complex constructors out of the box.

クラスの`Rebase`とクラスの`Redefinition`の場合、コンストラクターは当然ながら単純に保持されるため、`ConstructorStrategy`の仕様は廃止されます。代わりに、これらの保持されたコンストラクター (およびメソッド) 実装をコピーするには、これらのコンストラクター定義を含む元のクラス ファイルの検索を可能にする`ClassFileLocator`を指定する必要があります。 `Byte Buddy`は、元のクラスファイルの場所を独自に識別するために最善を尽くします※。例えば、対応する `ClassLoader`をクエリするか、アプリケーションのクラスパスを調べることによって。ただし、通常の`ClassLoader`を扱う場合、検索がまだ成功しない可能性があります。そのときは、カスタム`ClassFileLocator`を提供できます。


### Calling a default method

With its version 8 release, the Java programming language introduced default methods for interfaces. In Java, a default method invocation is expressed by a similar syntax to the invocation of a super method. As an only disparity, a default method invocation names the interface that defines the method. This is necessary because a default method invocation can be ambiguous if two interfaces define a method with identical signature. Accordingly, Byte Buddy's DefaultMethodCall implementation takes a list of prioritized interfaces. When intercepting a method, the DefaultMethodCall invokes a default method on the first-mentioned interface. As an example, assume that we wanted to implement the two following interfaces:

```java
interface First {
  default String qux() { return "FOO"; }
}
 
interface Second {
  default String qux() { return "BAR"; }
}
```

If we now created a class that implements both interfaces and implemented the qux method to call a default method, this invocation could express both a call of the default method defined on the First or the Second interface. However, by specifying the DefaultMethodCall to prioritize the First interface, Byte Buddy would know that it should invoke this latter interface's method instead of the alternative.

```java
new ByteBuddy(ClassFileVersion.JAVA_V8)
  .subclass(Object.class)
  .implement(First.class)
  .implement(Second.class)
  .method(named("qux")).intercept(DefaultMethodCall.prioritize(First.class))
  .make()
```

Note that any Java class that is defined in a class file version before Java 8 does not support default methods. Furthermore, you should be aware that Byte Buddy imposes weaker requirements on the invokability of a default method compared to the Java programming language. Byte Buddy only requires a default method's interface to be implemented by the most-specific class in a type's hierarchy. Other than the Java programming language, it does not require this interface to be the most specific interface that is implemented by any super class. Finally, if you do not expect a ambiguous default method definitions, you can always use DefaultMethodCall.unambiguousOnly() for receiving an implementation which throws an exception on the discovery of an ambiguous default method invocation. This same behavior is displayed by a prioritizing DefaultMethodCall where a default method call is ambiguous between non-prioritized interfaces and no prioritized interface was found to define a method with a compatible signature.

### Calling a specific method

In some cases, the above Implementations are not sufficient to implement more custom behavior. For example, one might want to implement a custom class with explicit behavior. For example, we might want to implement the following Java class with a constructor that does not have a super constructor with identical arguments:

```java
public class SampleClass {
  public SampleClass(int unusedValue) {
    super();
  }
}
```

The previous SuperMethodCall implementation could not be used to implement this class as the Object class does not define a constructor that takes an int as its parameter. Instead, we can invoke the Object super constructor explicitly:

```java
new ByteBuddy()
  .subclass(Object.class, ConstructorStrategy.Default.NO_CONSTRUCTORS)
  .defineConstructor(Arrays.<Class<?>>asList(int.class), Visibility.PUBLIC)
  .intercept(MethodCall.invoke(Object.class.getDeclaredConstructor()))
  .make()
```

With the above code, we have created a simple subclass of Object that defines a single constructor which takes a single int parameter that is not used. The latter constructor is then implemented by an explicit method call to the Object super constructor.

The MethodCall implementation can also be used when passing arguments. These arguments are either passed explicitly as a value, as a value for an instance field that needs to be set manually or as a given parameter value. Also, the implementation allows to invoke methods on other instances than the one being instrumented. Furthermore, it allows for the construction of new instances to be returned from an intercepted method. The documentation of the MethodCall class provides detailed information on these features.

### Accessing fields

Using the FieldAccessor, it is possible to implement a method to read or to write a field value. In order to be compatible to this implementation, a method must either:

- Have a signature similar to void setBar(Foo f) to define a field setter. The setter will normally access a field named bar, as it is conventional in the Java bean specification. In this context, the parameter type Foo must be a sub type this field's type.
- Have a signature similar to Foo getBar() to define a field getter. The setter will normally access a field named bar, as it is conventional in the Java bean specification. For this to be possible, the method's return type Foo must be a super type of the field's type.

Creating such an implementation is trivial: Simply call FieldAccessor.ofBeanProperty(). However, if you do not want to derive a field's name from a method's name, you can still specify the field name explicitly by using FieldAccessor.ofField(String). Using this method, the only argument defines the field's name that should be accessed. If required, this even allows you to define a new field if such a field does not yet exist. When accessing an existing field, you are able to specify the type in which a field is defined by calling the in method. In Java, it is legal to define a field in several classes of a hierarchy. In the process, a field of a class is shadowed by the field definition in its subclass. Without such an explicit location of the field's class, Byte Buddy will access the first field it encounters by traversing through a class hierarchy, starting with the most specific class.

Let us look at an example application of the FieldAccessor. For this example, we assume that we receive some UserType that we want to subclass at runtime. For this purpose, we want to register an Interceptor for each instance which is represented by an interface. This way, we are able to provide different implementations according to our actual requirement. This latter implementation should then be exchangeable by calling methods of the InterceptionAccessor interface on the corresponding instance. In order to create instances of this dynamic type, we further do not want to use reflection but call a method of an InstanceCreator which serves as an object factory. The following types resemble this setup:

```java
class UserType {
  public String doSomething() { return null; }
}
 
interface Interceptor {
  String doSomethingElse();
}
 
interface InterceptionAccessor {
  Interceptor getInterceptor();
  void setInterceptor(Interceptor interceptor);
}
 
interface InstanceCreator {
  Object makeInstance();
}
```

We already learned how to intercept methods of a class by using a MethodDelegation. Using the latter implementation, we can define a delegation to an instance field and name this field interceptor. Additionally, we are implementing the InterceptionAccessor interface and intercept all methods of the interface to implement accessors of this field. By defining a bean property accessor, we achieve a getter for getInterceptor and a setter for setInterceptor:

```java
Class<? extends UserType> dynamicUserType = new ByteBuddy()
  .subclass(UserType.class)
    .method(not(isDeclaredBy(Object.class)))
    .intercept(MethodDelegation.toField("interceptor"))
  .defineField("interceptor", Interceptor.class, Visibility.PRIVATE)
  .implement(InterceptionAccessor.class).intercept(FieldAccessor.ofBeanProperty())
  .make()
  .load(getClass().getClassLoader())
  .getLoaded();
```

With the new dynamicUserType, we can implement the InstanceCreator interface to become a factory of this dynamic type. Again, we are using the already known MethodDelegation to call the dynamic type's default constructor:

```java
InstanceCreator factory = new ByteBuddy()
  .subclass(InstanceCreator.class)
    .method(not(isDeclaredBy(Object.class)))
    .intercept(MethodDelegation.construct(dynamicUserType))
  .make()
  .load(dynamicUserType.getClassLoader())
  .getLoaded().newInstance();
```

Note that we need to use the dynamicUserType's class loader to load the factory. Otherwise, this type would not be visible to the factory when it is loaded.

With these two dynamic types, we can finally create a new instance of the dynamically enhanced UserType and define custom Interceptors for its instances. Let us conclude this example by applying some HelloWorldInterceptor to a freshly created instance. Note how we are now able to do this without using any reflection, thanks to both the field accessor interface and the factory.

```java
class HelloWorldInterceptor implements Interceptor {
  @Override
  public String doSomethingElse() {
    return "Hello World!";
  }
}
 
UserType userType = (UserType) factory.makeInstance();
((InterceptionAccessor) userType).setInterceptor(new HelloWorldInterceptor());
```

### Miscellaneous

Additionally to the Implementations we discussed so far, Byte Buddy includes several other implementations:

- A StubMethod implements a method to simply return the method return type's default value without any further action. This way, a method call can be silently suppressed. This approach can for example be used to implement mock types. The default value of any primitive type is zero or the zero character respectively. Methods that return a reference type return null as their default.
- The ExceptionMethod can be used to implement a method to only throw an exception. As mentioned before, it is possible to throw checked exceptions from any method even if a method does not declare this exception.
- The Forwarding implementations allows to simply forward a method call to another instance of the same type as the declaring type of an intercepted method. The same result can be achieved using a MethodDelegation. However, by Forwarding a simpler delegation model is applied which can cover use cases where no target method discovery is required.
- The InvocationHandlerAdapter allows to use existing InvocationHandlers from the for the proxy classes that ship with the Java Class Library.
- The InvokeDynamic implementation allows to bind a method dynamically at runtime using a bootstrap methods which are accessible from Java 7 on.

## Annotations

Byte Buddy が機能の一部を提供するためにアノテーションにどのように依存しているかを学びました。 また、アノテーションベースの API を備えた Java アプリケーションは Byte Buddy だけではありません。動的に作成された型をそのようなアプリケーションと統合するために、Byte Buddy では、作成された型とそのメンバーのアノテーションを定義できます。 動的に作成された型へのアノテーションの割り当ての詳細を検討する前に、ランタイム クラスにアノテーションを付ける例を見てみましょう。

```java
@Retention(RetentionPolicy.RUNTIME)
@interface RuntimeDefinition { }
 
class RuntimeDefinitionImpl implements RuntimeDefinition {
  @Override
  public Class<? extends Annotation> annotationType() {
    return RuntimeDefinition.class;
  }
}
 
new ByteBuddy()
  .subclass(Object.class)
  .annotateType(new RuntimeDefinitionImpl())
  .make();
```

As insinuated by Java's @interface keyword, annotations are internally represented as interface types. As a consequence, annotations can be implemented by a Java class just like an ordinary interface. The only difference to implementing an interface is an annotation's implicit annotationType method which determines the annotation type a class represents. The latter method usually returns the implemented annotation type's class literal. Other than that, any annotation property is implemented as if it was an interface method. However, note that an annotation's default values need to be repeated by an annotation method's implementation.

Defining annotations for a dynamically created class can be in particularly important when a class should serve as a subclass proxy for another class. A subclass proxy is often used to implement cross-cutting concerns where the subclass should mimic the original class as transparently as possible. However, annotations on a class are not retained for its subclasses as long as this behavior is explicitly required by defining an annotation to be @Inherited. Using Byte Buddy, creating subclass proxies that retain their base class's annotations is easy by invoking the attribute method of Byte Buddy's domain specific language. This methods expects a TypeAttributeAppender as its argument. A type attribute appender offers a flexible way for defining the annotations of a dynamically created class, based on its base class. For example, by passing a TypeAttributeAppender.ForSuperType, a class's annotations are copied to its dynamically created subclasses. Note that annotations and type attribute appenders are additive and no annotation type must be defined more than once for any class.

Method and field annotations are defined similarly to type annotations which we just discussed. A method annotation can be defined as a conclusive statement in Byte Buddy's domain specific language for implementing a method. Likewise, a field can be annotated after its definition. Again, let us look at an example:

```java
new ByteBuddy()
  .subclass(Object.class)
    .annotateType(new RuntimeDefinitionImpl())
  .method(named("toString"))
    .intercept(SuperMethodCall.INSTANCE)
    .annotateMethod(new RuntimeDefinitionImpl())
  .defineField("foo", Object.class)
    .annotateField(new RuntimeDefinitionImpl())
```

The above code example overrides the toString method and annotates the overridden method with RuntimeDefinition. Furthermore, the created type defines a field foo that carries the same annotation and also defines the latter annotation on the created type itself.

By default, a ByteBuddy configuration does not predefine any annotations for a dynamically created type or type member. However, this behavior can be altered by providing a default TypeAttributeAppender, MethodAttributeAppender or FieldAttributeAppender. Note that such default appenders are not additive but replace their former values.

Sometimes, it is desirable to not load an annotation type or the types of any of its properties when defining a class. For this purpose, it is possible to use the AnnotationDescription.Builder which offers a fluent interface for defining an annotation without triggering class loading but at the costs of type safety. All annotation properties are however evaluated at runtime.

By default, Byte Buddy includes any property of an annotation into a class file, including default properties that are specified implicitly by a default value. This behavior can however be customized by providing an AnnotationFilter to a ByteBuddy instance.

### Type annotations

Byte Buddy exposes and writes type annotations as they were introduced as a part of Java 8. Type annotations are accessible as declared annotations by the any TypeDescription.Generic instance. If a type annotation should be added to a type of a generic field or method, an annotated type can be generated using a TypeDescription.Generic.Builder.

### Attribute appenders

A Java class file can include any custom information as a so-called attribute. Such attributes can be included using Byte Buddy by using an *AttributeAppender for a type, field or method. Attribute appenders can however also be used for defining methods based on information that is provided by the intercepted type, field or method. For example, it is possible to copy all annotations of an intercepted method when overriding a method in a subclass:

```java
class AnnotatedMethod {
  @SomeAnnotation
  void bar() { }
}
new ByteBuddy()
  .subclass(AnnotatedMethod.class)
  .method(named("bar"))
  .intercept(StubMethod.INSTANCE)
  .attribute(MethodAttributeAppender.ForInstrumentedMethod.INSTANCE)
```

The above code overrides the bar method of the AnnotatedMethod class but copies all annotations of the overridden method, including annotations on parameters or types.

When a class is redefined or rebased, the same rule might not apply. By default, ByteBuddy is configured to preserve any annotations of a rebased or redefined method, even if the method is intercepted as above. This behavior can however be changed such that Byte Buddy discards any preexisting annotations by setting the AnnotationRetention strategy to DISABLED.

## Custom method implementations

In the previous sections, we described Byte Buddy's standard API. None of the features described so far requires knowledge or the explicit expression of Java byte code. However, if you require to create custom byte code, you can do so by directly accessing the API of ASM, a low-level byte code library on top of which Byte Buddy is built. However, note that different versions of ASM are not compatible to another such that you need to repackage Byte Buddy into your own namespace when releasing your code. Otherwise, your application might introduce incompatibilities to other uses of Byte Buddy when another dependency is expecting a different version of Byte Buddy which is based on a different version of ASM. You can find detailed information on maintaining a dependency on Byte Buddy on the front page.

The ASM library comes with an excellent documentation of Java byte code and on the use of the library. Therefore, we want to refer you to this documentation in case that you want to learn in detail about Java byte code and ASM's API. Instead, we are only going to provide a brief introduction to the JVM's execution model and Byte Buddy's adaption of ASM's API.

Any Java class file is constituted by several segments. The core segments can be categorized roughly as follows:

- Base data: A class file references the class's name as well as the name of its superclass and its implemented interfaces. Additionally, a class file contains different meta data as for example the class's Java version number, its annotations or the name of the source file the compiler processed for creating the class.
- Constant pool: A class's constant pool is a collection of values that are referenced by a member or an annotation of this class. Among those values, the constant pool stores for example primitive values and strings that are created by some literal expression in the class's source code. Furthermore, the constant pool stores the names of all types and methods that are used within the class.
- Field list: A Java class file contains a list of all fields that are declared in this class. Additionally to the type, name and the modifiers of a field, the class file stores the annotations of each field.
- Method list: Similar to the list of fields, a Java class file contains a list of all declared methods. Other than fields, non-abstract methods are additionally described by an array of byte-encoded instructions that describe the method body. These instructions represent the so-called Java byte code.

Fortunately, the ASM library takes full responsibility of establishing an applicable constant pool when creating a class. With this, the only non-trivial element remains the description of a method's implementation which is represented by an array of execution instructions, each encoded as a single byte. These instructions are processed by a virtual stack machine on the method's invocation. For a simple example, let us consider a method that calculates and returns the sum of the two primitive integers 10 and 50. This method's Java byte code would look as follows:

```java
LDC     10  // stack contains 10
LDC     50  // stack contains 10, 50
IADD        // stack contains 60
IRETURN     // stack is empty
```

The above mnemonic of an array of Java byte code starts off by pushing both numbers onto the stack by using the LDC instruction. Note how this execution order differs from the order that is expressed in Java source code where the addition would be written as the infix notation 10 + 50. However, the latter order cannot be processed by a stack machine where any instruction like + can only access the upper most values that are currently found on the stack. This addition is expressed by IADD which consumes the two upmost stack values which it both expects to be primitive integers. In the process, it adds these two values and pushes the result back onto the top of the stack. Finally, the IRETURN statement consumes this calculation result and returns it from the method, leaving us with an empty stack.

We already mentioned that any primitive value that is referenced in a method is stored in the class's constant pool. This is also true for the numbers 50 and 10 which are referenced in the above method. Any value in the constant pool is assigned an index with the length of two bytes. Let us assume that the numbers 10 and 50 were stored at the indexes 1 and 2. Together with the byte values of the above mnemonic which are 0x12 for LDC, 0x60 for IADD and 0xAC for IRETURN, we now know how express the above method as raw byte instructions:

```text
12 00 01
12 00 02
60
AC
```

For a compiled class, this exact byte sequence could be found in the class file. However, this description does not yet suffice to fully define a method's implementation. In order to accelerate a Java application's runtime execution, each method is required to inform the Java virtual machine about the required size for its execution stack. For the above method which comes without branches, this is rather easy to determine as we already saw that there will be at most two values on the stack. However, for more complex methods, providing this information can easily become a complex task. And to make things worse, stack values can be of different size. Both long and double values consume two slots while any other value consumes one. As if this wasn't enough, the Java virtual machine also requires information about the size of all local variables within a method's body. All such variables in a method are stored in an array which also includes any method parameter and the this reference for non-static methods. Again, long and double values consume two slots in this array.

Evidently, keeping track of all this information makes the manual assembly of Java byte code tedious and error-prone which is why Byte Buddy provides a simplifying abstraction. Within Byte Buddy, any stack instruction is contained by an implementation of the StackManipulation interface. Any implementation of a stack manipulation combines an instruction to alter a given stack and information on the size impact of this instruction. Any number of such instructions can then easily be conflated to a common instruction. To demonstrate this, let us first implement a StackManipulation for the IADD instruction:

```java
enum IntegerSum implements StackManipulation {
 
  INSTANCE; // singleton
 
  @Override
  public boolean isValid() {
    return true;
  }
 
  @Override
  public Size apply(MethodVisitor methodVisitor,
                    Implementation.Context implementationContext) {
    methodVisitor.visitInsn(Opcodes.IADD);
    return new Size(-1, 0);
  }
}
```

From the above apply method, we learn that this stack manipulation executes the IADD instruction by invoking the related method on ASM's method visitor. Furthermore, the method expresses that the instruction reduces the current stack Size by one slot. The second argument of the created Size instance is 0 what expresses that this instruction does not require a specific minimal stack size for calculating interim results. Furthermore, any StackManipulation can express to be invalid. This behavior can be used for more complex stack manipulation as for example object assignments which might break a type constraint. We will look at an example of an invalid stack manipulation later in this section. Finally, note that we describe the stack manipulation as a singleton enumeration. Using such immutable, functional descriptions of stack manipulations proved to be a good practice for Byte Buddy's internal implementation and we can only recommend you to follow the same approach.

By combining the above IntegerSum with the predefined IntegerConstant and the MethodReturn stack manipulations, we can now implement a method. Within Byte Buddy, a method implementation is contained by a ByteCodeAppender which we implement as follows:

```java
enum SumMethod implements ByteCodeAppender {
 
  INSTANCE; // singleton
 
  @Override
  public Size apply(MethodVisitor methodVisitor,
                    Implementation.Context implementationContext,
                    MethodDescription instrumentedMethod) {
    if (!instrumentedMethod.getReturnType().asErasure().represents(int.class)) {
      throw new IllegalArgumentException(instrumentedMethod + " must return int");
    }
    StackManipulation.Size operandStackSize = new StackManipulation.Compound(
      IntegerConstant.forValue(10),
      IntegerConstant.forValue(50),
      IntegerSum.INSTANCE,
      MethodReturn.INTEGER
    ).apply(methodVisitor, implementationContext);
    return new Size(operandStackSize.getMaximalSize(),
                    instrumentedMethod.getStackSize());
  }
}
```

Again, the custom ByteCodeAppender is implemented as a singleton enumeration.

Before implementing the desired method, we first validate that the instrumented method really returns a primitive integer. Otherwise, the created class would be rejected by the JVM's validator. Then we load the two numbers 10 and 50 onto the execution stack, apply the summation of these values and return the calculation result. By wrapping all these instructions with a compound stack manipulation, we can conclusively retrieve the aggregated stack size that is required to perform this chain of stack manipulations. Finally, we return the overall size requirements of this method. The first argument of the returned ByteCodeAppender.Size reflects the required size for the execution stack which we just mentioned to be contained by the StackManipulation.Size. Additionally, the second argument reflects the required size for the local variable array which here simply resembles the required size for the method's parameters and a possible this reference since we did not define any local variables of our own.

With this implementation of our summation method, we are now ready to provide a custom Implementation for this method which we can provide to Byte Buddy's domain specific language:

```java
enum SumImplementation implements Implementation {
 
  INSTANCE; // singleton
 
  @Override
  public InstrumentedType prepare(InstrumentedType instrumentedType) {
    return instrumentedType;
  }
 
  @Override
  public ByteCodeAppender appender(Target implementationTarget) {
    return SumMethod.INSTANCE;
  }
}
```

Any Implementation is queried in two stages. First, an implementation gets the chance to alter the created class by adding additional fields or methods in the prepare method. Furthermore, the preparation allows an implementation to register a TypeInitializer which we learned about in a previous section. If no such preparations are required, it suffices to return the unaltered InstrumentedType which is provided as the argument. Note that an Implementation should not normally return an individual instance of an instrumented type but should call the instrumented type's appender methods which are all prefixed by with. After any Implementation for a particular class creation is prepared, the appender method is invoked for retrieving a ByteCodeAppender. This appender is then queried for any method that was selected for interception by the given implementation and also for any method that was registered during the implementation's invocation of the prepare method.

Note that Byte Buddy only invokes each Implementation's prepare and appender methods a single time during the creation process of any class. This is guaranteed no matter how many times an implementation is registered for use in a class's creation. This way, an Implementation can avoid to verify if a field or method is already defined. In the process, Byte Buddy compares Implementations instances by their hashCode and equals methods. In general, any class that is used by Byte Buddy should provide meaningful implementations of these methods. The fact that enumerations come with such implementations per definition is another good reason for their use.

With all this, let us see the SumImplementation in action:

```java
abstract class SumExample {
  public abstract int calculate();
}
 
new ByteBuddy()
  .subclass(SumExample.class)
    .method(named("calculate"))
    .intercept(SumImplementation.INSTANCE)
  .make()
```

Congratulations! You just extended Byte Buddy to implement a custom method that computes and returns the sum of 10 and 50. Of course, this example implementation is not of much practical use. However, more complex implementations can be implemented easily on top of this infrastructure. After all, if you feel that you created something handy, please consider to contribute your implementation. We are looking forward to hearing from you!

Before we move on to customizing some other components of Byte Buddy, we should briefly discuss the use of jump instructions and the matter of the so-called Java stack frames. Since Java 6, any jump instruction which are used to implement for example the if or the while statements require some additional information in order to accelerate the JVM's verification process. This additional information is called a stack map frame. A stack map frame contains information about all values that are found on the execution stack at any target of a jump instruction. By providing this information, the JVM's verifier saves some work which is now however now left to us. For more complex jump instructions, providing correct stack map frames is a rather difficult task and many code generation frameworks have quite some trouble to always create correct stack map frames. So how do we deal with this matter? As a matter of fact, we simply don't. It is Byte Buddy's philosophy that code generation should only be used as the glue between a type hierarchy that is unknown at compile time and custom code that needs to be injected into these types. The actual code that is generated should therefore remain as confined as possible. Wherever possible, conditional statements should rather be implemented and compiled in a JVM language of your choice and then be bound to a given method by using a minimalistic implementation. A nice side effect of this approach is that Byte Buddy's users can work with normal Java code and use their accustomed tools like debuggers or IDE code navigators. None of this would be possible with generated code which does not have a source code representation. However, if you really need to create byte code with jump instructions, make sure to add the correct stack map frames using ASM since Byte Buddy will not automatically include them for you.

### Creating a custom assigner

In a previous section, we discussed that Byte Buddy's built in Implementations rely on an Assigner in order to assign values to variables. In this process, an Assigner is able to apply a transformation of one value to another by emitting an appropriate StackManipulation. Doing so, Byte Buddy's built-in assigners provide for example auto-boxing of primitive values and their wrapper types. In the most trivial case, a value is assignable to a variable as is. In some cases, an assignment may however not be possible at all what can be expressed by returning an invalid StackManipulation from an assigner. A canonical implementation of an invalid assignment is provided by Byte Buddy's IllegalStackManipulation class.

To demonstrate the use of a custom assigner, we are now going to implement an Assigner that only assigns values to String-typed variables by calling the toString method on any value it receives:

```java
enum ToStringAssigner implements Assigner {
 
  INSTANCE; // singleton
 
  @Override
  public StackManipulation assign(TypeDescription.Generic source,
                                  TypeDescription.Generic target,
                                  Assigner.Typing typing) {
    if (!source.isPrimitive() && target.represents(String.class)) {
      MethodDescription toStringMethod = new TypeDescription.ForLoadedType(Object.class)
        .getDeclaredMethods()
        .filter(named("toString"))
        .getOnly();
      return MethodInvocation.invoke(toStringMethod).virtual(sourceType);
    } else {
      return StackManipulation.Illegal.INSTANCE;
    }
  }
}
```

The above implementation first validates that the input value is not of a primitive type and that the target variable type is of a String type. If these conditions are not fulfilled, the Assigner emits an IllegalStackManipulation to render the attempted assignment invalid. Otherwise, we identify the Object type's toString method by its name. We then use Byte Buddy's MethodInvocation to create a StackManipulation that calls this method virtually on the source type. Finally, we can integrate this custom Assigner with for example Byte Buddy's FixedValue implementation as follows:

```java
new ByteBuddy()
  .subclass(Object.class)
  .method(named("toString"))
    .intercept(FixedValue.value(42)
      .withAssigner(new PrimitiveTypeAwareAssigner(ToStringAssigner.INSTANCE),
                    Assigner.Typing.STATIC))
  .make()
```

When the toString method is called on an instance of the above type, it will return the string value 42. This is only possible by using our custom assigner which converts the Integer type to a String by invoking the toString method. Note that we additionally wrapped the custom assigner with the built-in PrimitiveTypeAwareAssigner which performs an auto-boxing of the provided primitive int to its wrapper type before delegating the assignment of this wrapped primitive value to its inner assigner. Other built-in assigners are the VoidAwareAssigner and the ReferenceTypeAwareAssigner. Always remember to implement meaningful hashCode and equals methods for your custom assigners since those methods are normally called from their counterparts in the Implementation that is making use of a given assigner. Again, by implementing an assigner as a singleton enumeration, we avoid doing this manually.

### Creating a custom parameter binder

We already mentioned in a previous section that it is possible to extend the MethodDelegation implementation to process user-defined annotations. For this purpose, we need to provide a custom ParameterBinder which knows how to handle a given annotation. As an example, we want to define an annotation with the purpose to simply inject a fixed string into the annotated parameter. First, we define such a StringValue annotation:

```java
@Retention(RetentionPolicy.RUNTIME)
@interface StringValue {
  String value();
}
```

We need to make sure that the annotation is visible at runtime by setting the appropriate RuntimePolicy. Otherwise, the annotation is not retained at runtime and Byte Buddy does not have a chance to discover it. Doing so, the above value property contains the string which is assigned to the annotated parameter as a value.

With our custom annotation, we need to create a corresponding ParameterBinder which is able to create a StackManipulation which expresses the binding for this parameter. This parameter binder is invoked each time, its corresponding annotation is discovered on a parameter by the MethodDelegation. Implementing a custom parameter binder for our example annotation is straight forward:

```java
enum StringValueBinder
    implements TargetMethodAnnotationDrivenBinder.ParameterBinder<StringValue> {
 
  INSTANCE; // singleton
 
  @Override
  public Class<StringValue> getHandledType() {
    return StringValue.class;
  }
 
  @Override
  public MethodDelegationBinder.ParameterBinding<?> bind(AnnotationDescription.Loaded<StringValue> annotation,
                                                         MethodDescription source,
                                                         ParameterDescription target,
                                                         Implementation.Target implementationTarget,
                                                         Assigner assigner,
                                                         Assigner.Typing typing) {
    if (!target.getType().asErasure().represents(String.class)) {
      throw new IllegalStateException(target + " makes illegal use of @StringValue");
    }
    StackManipulation constant = new TextConstant(annotation.loadSilent().value());
    return new MethodDelegationBinder.ParameterBinding.Anonymous(constant);
  }
}
```

Initially, the parameter binder makes sure that the target parameter is actually a String type. If this is not the case, we will throw an exception to inform the annotation's user of his illegal placing of this annotation. Otherwise, we simply create a TextConstant which represents the loading of a constant pool string onto the execution stack. This StackManipulation is then wrapped as an anonymous ParameterBinding which is finally returned from the method. Alternatively, you could have provided either a Unique or an Illegal parameter binding. A unique binding is identified by any object which allows to retrieve this binding from an AmbiguityResolver. In a later step, such a resolver is able to look up if a parameter binding was registered with some unique identifier and can then decide if this binding is superior to another successfully bound method. With an illegal binding, one can instruct Byte Buddy that a specific pair of a source and a target method is incompatible and cannot be bound together.

This already is all the information that is required for using a custom annotation with a MethodDelegation implementation. After receiving a ParameterBinding, it makes sure that its value is bound to the correct parameter or it will discard the current pair of a source and target method as unbindable. Furthermore, it will allow AmbiguityResolvers to look up unique bindings. Finally, let us put this custom annotation in action:

```java
class ToStringInterceptor {
  public static String makeString(@StringValue("Hello!") String value) {
    return value;
  }
}
 
new ByteBuddy()
  .subclass(Object.class)
  .method(named("toString"))
    .intercept(MethodDelegation.withDefaultConfiguration()
      .withBinders(StringValueBinder.INSTANCE)
      .to(ToStringInterceptor.class))
  .make()
```

Note that by specifying the StringValueBinder as the only parameter binder, we replace all defaults. Alternatively, we could have appended the parameter binder to those that are already registered. With only one possible target method in the ToStringInterceptor, the dynamic class's intercepted toString method is bound to the latter method's invocation. When the target method is invoked, Byte Buddy assigns the annotation's string value as the target method's only parameter.
